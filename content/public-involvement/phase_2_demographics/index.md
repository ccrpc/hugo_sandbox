---
title: "Public Outreach - Phase 2 Respondent Demographics"
draft: false
weight: 25
---

## Introduction
Two rounds of public input were conducted via a SurveyMonkey survey linked to in the project website at ccrpc.org/florida. The two surveys featured questions on respondents’ use and access to different transportation modes, questions about their support for and/or concern over the proposed improvements, as well as optional questions about demographics and location of residency.  The first online survey was open to the public from June 30, 2021 to July 31, 2021. The second online survey was open to the public April 20, 2022 to May 11, 2022.

The following methods were used to make community members aware of the website and both surveys:
- Posting on the CUUATS Facebook, Instagram, and Twitter
- Sending the social media posts, digital flyers, and info blurbs to community partners for them to distribute. Partners contacted include:
    - CCRPC Communications office
    - CUMTD
    - City of Urbana
    - Urbana Parks District
    - PACE
    - The SUNA and WUNA neighborhood groups
    - UIUC Facilities, UIUC University Housing
- Installing yard signs in the public right of way along the Florida Avenue corridor
- Hanging door hangers at all homes directly on the corridor
- Discussing the study with reporters from The News-Gazette and WICS
- Tabling in Orchard Downs during the annual Bike to Work Day

In addition to survey responses, direct correspondence with CUUATS staff about the study (including emails, mail, and phone calls) was documented and incorporated into the written feedback record (this was limited; non-survey responses totaled less than 10 responses). The public outreach process resulted in the following number of responses from the online survey, emails, or other means:
- **Phase One** - 434 documented responses (427 from the online survey)
- **Phase Two** - 119 documented responses

**The following summarizes Phase 2 respondent demographics:**


### Area of Residence

Of the 119 respondents, 107 provided the nearest intersection to their home. See the tables and maps below.

{{<image src="Response_Map.png"
  alt="Map showing one dot for each survey response, across entire urban area"
  caption="All survey responses in the C-U area"
  position="right">}}

{{<image src="Location_Chart.PNG"
  alt="Table showing survey responses by location within the urban area"
  caption="Survey responses by location"
  position="right">}}


This shows that responses came from across the Champaign-Urbana urbanized area, with the largest concentration in southwest Urbana around the corridor itself. Phase Two reported a higher percentage of respondents from the influence area, with 32% living in the study area and 35% within the corridor area of influence. 

According to the 2010 Census, 2,260 people lived in the blocks adjacent to the Study Area, and 4,345 lived in the blocks in the Area of Influence. As mentioned in the Transportation section of Existing Conditions, the most recent traffic count data from 2016 found an average daily traffic of 10,300 for the Lincoln to Orchard segment, 9,300 for Orchard to Race, and 8,800 for Race to Vine. Comparing these resident and traffic counts, we see that although the residents of the corridor are deeply affected by the road and should be considered in any proposals, they are a fraction of the overall users, and their perspectives must be considered alongside the context of other community needs.



### Age

The greatest proportion of responses came from residents above the age of 40, with this cohort representing approximately 51% of survey respondents. Comparing this to the population demographics of the Area of Influence and Champaign County, we can see that the 40-59 and 60-79 cohorts were significantly overcounted, while the 80+ cohort was slightly overcounted when compared to the area of influence but undercounted when compared to the county at large. 
Additionally, the 20-39 cohort was overcounted, with 45% of respondents falling into the age group. This is an increase in representation from Phase One of the survey, which reported 29% of respondents in this age group. The under 20 cohort remained a small fraction of respondents at 3.5%. As stated previously, these statistics could be due to a variety of factors. However, this is a noteworthy increase from Phase One of the survey, which reported less than 1% of respondents under 20 years of age.

 <rpc-chart url="age_phase2.csv"
  chart-title="Age of Respondents"
  type="horizontalBar"
  y-label="Age"
  x-label="Percent of Respondents"></rpc-chart>


### Gender

Approximately 49% of respondents identify as female, 45% identify as male, 3.5% identify as non-binary, and 2.6% prefer to self-describe their gender identity. 

<rpc-chart url="gender_phase2.csv"
  chart-title="Gender of Respondents"
  type="horizontalBar"
  y-label="Employment Status"
  x-label="Percent of Respondents"
  x-min="0"
  x-max="100"></rpc-chart>

### Race and Ethnicity

Non-Hispanic white residents comprised the vast majority of respondents, with 89% of responses from this group. Next, Asian respondents and respondents who identified their race or ethnicity as “Other” each accounted for 4.6% of respondents. Residents of Hispanic/Latino ethnicity comprised of 2.7% of respondents, and African American or Black made up 1.8%. American Indian or Alaskan Native made up less than 1% of respondents. Compared to local ACS data, we can see that white residents are significantly overcounted, as they make up 65% of AoI residents and 72% of county residents. 

<rpc-chart url="race_phase2.csv"
  chart-title="Race and Ethnicity of Respondents"
  type="horizontalBar"
  y-label="Race or Ethnicity"
  x-label="Percent of Respondents"></rpc-chart>

### Employment

Nearly 75% of respondents work outside the home, 13% are retired, 18% work inside the home, 10% are students, and less than 3% each are homemakers or those looking for work.

<rpc-chart url="Employment_phase2.csv"
  chart-title="Employment Status of Respondents"
  type="horizontalBar"
  y-label="Employment Status"
  x-label="Percent of Respondents"
  x-min="0"
  x-max="100"></rpc-chart>








