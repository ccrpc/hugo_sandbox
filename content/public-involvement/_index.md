---
title: "Public Involvement"
draft: false
menu: main
weight: 50
bannerHeading: "Get Involved!"
bannerText: >
  No one knows the community like those who live, work, and go to school here. Get involved in Florida Avenue Corridor Study to share your input! 
---

The Champaign-Urbana Urbanized Area Transportation Study is evaluating one mile
of Florida Avenue in Urbana (from Lincoln Avenue to Vine Street) in partnership
with the City of Urbana, UIUC, and MTD. The goal of this study is to identify
ways to increase transportation safety and mobility in this high-traffic
corridor. Your input is needed to identify the challenges and opportunities
people experience while traveling on Florida Avenue. Your input will inform the
future vision for Florida Avenue and help increase mobility in the Champaign-Urbana
area. 

Throughout the planning process, this website will be the one-stop-shop for
project information and public involvement opportunities.

Click the links below to see further information about our past, current, and future public outreach efforts!




