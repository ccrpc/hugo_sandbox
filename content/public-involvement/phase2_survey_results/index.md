---
title: "Public Outreach - Phase Two Summary"
draft: false
weight: 15
---

## Introduction

The second round of feedback was conducted via a SurveyMonkey survey linked to in the project website at ccrpc.org/florida. This survey featured questions on respondent’s use and access to different transportation modes, questions about their support for and/or concern over the proposed improvements, as well as optional questions about demographics and location of residency. The online survey was open to the public from April 20, 2022 to May 11, 2022.
The following methods were used to make community members aware of the website and survey:

- Posting on the CUUATS Facebook, Instagram, and Twitter
- Sending the social media posts, digital flyers, and info blurbs to community partners for them to distribute. Partners contacted include: CCRPC Communications office, CUMTD, City of Urbana, Urbana Parks District, PACE, The SUNA and WUNA neighborhood groups, UIUC Facilities, and UIUC University Housing.
- Installing yard signs in the public right of way along the Florida Avenue corridor

The second round of the public outreach process resulted in 119 documented responses. The following sections illustrate these responses in more detail:
1.	Respondent Transportation and Access
2.	Support and Concern

## Respondent Transportation and Access
Respondents were asked two questions regarding their use of and access to various modes of transportation along Florida Avenue between Lincoln Avenue and Vine Street.

### How often do you use each of the following transportation modes on Florida Avenue between Lincoln Avenue and Vine Street?

The results show private motor vehicles as the largest mode of transportation, with approximately 85% of respondents utilizing this mode at least once a week. Additionally, a significant percentage of respondents reported walking and biking along the corridor. See the table below for more information.    

![Chart showing respondents' modes of transportation.](./transit_mode.png)


### Do you own or have reliable access to a …?  (check all that apply)

The results show that approximately 92% of respondents have access to a private motor vehicle, making it the most accessible mode of travel among resident respondents. Additionally, about 80% of respondents have access to a bicycle and 47% to a bus pass. See the table below for more information.    

![Chart showing respondents' access to modes of transportation.](./access_mode.png)

## Support and Concerns
Respondents were asked to provide their opinion for each of the proposed improvements to the Florida Avenue Corridor by sharing whether they support the improvements or have concerns about the proposed ideas. Respondents were asked the following questions: 1) Which of the proposed improvements to the corridor do you support? and 2) Do you have concerns about any of the proposed improvements?

Given the opportunity to expand upon their answers to the previous questions, respondents provided comments pertaining to the project. Approximately 44% of the total respondents chose to share additional thoughts. 

The following are the responses and corresponding additional comments to the survey questions, organized in order of improvements receiving the most support/least concern to least support/most concern:

**1. Replace damaged sidewalk panels** throughout the corridor to improve pedestrian safety and accessibility.

**Support: 89%      Concern: 3%**

*Comments: Respondents expressed support for improving sidewalks for pedestrians. Many comments encouraged the replacement of entire stretches of sidewalk panels along the corridor, some respondents noting that they avoid using the sidewalk in its current state because it feels unsafe.*


**2.	Add an off-street shared-use path** on the south side of Florida Avenue between Lincoln Avenue and Race Street to improve safety and accessibility for pedestrians and cyclists.

**Support: 80%      Concern: 5%**

*Comments: Respondents expressed their desire for pedestrian and cyclist safety and most supported the off-street shared use path as an option, rather than an on-street bike lane. Respondents who shared concern over the off-street shared use path want to ensure that green space is not lost to the paved pathway.* 


**3.	Reconstruct the Florida Avenue roadway** from Lincoln Avenue to Vine Street to improve safety and accessibility for existing vehicle lanes and on-street bike lanes (including crosswalk repainting and sidewalk ramp reconstruction).

**Support: 88%       Concern: 6%**

*Comments: Respondents expressed support for resurfacing the corridor and increasing traffic flow. Many commented on the current state of the road as “crumbling” and in need of repair, as it can impact people’s safety while traveling.* 


**4.	Upgrade lighting** throughout the corridor to improve safety and accessibility for all users.

**Support: 64%      Concern: 8%**

*Comments: Respondents were in support of upgrading lighting throughout the corridor to improve safety and wanted to voice their desire for dark sky compliant lighting and light fixtures that matched the character of the neighborhood.*


**5.	Upgrade existing traffic signals** (including crosswalk signals) at the Florida Avenue intersections with Lincoln Avenue and Orchard Street to improve functionality.	

**Support: 56%      Concern: 10%**

*Comments: Respondents were in support for upgrading existing traffic signals to include improved pedestrian crossings, including flashing pedestrian crossing signals and stop signs similar to those on/near UIUC campus. Some suggested incorporating more signage that communicates to motorists about merging lanes and changing speed limits.*


**6.	Consolidate bus stops** between Lincoln Avenue and Vine Street to improve route efficiency by reducing the total number of bus stops in the corridor from 14 to 12.	

**Support: 49%      Concern: 11%**

*Comments: Some respondents were in favor of having frequent bus stops to ensure that they are easily accessible to riders, while others voiced support for increasing traffic flow through the corridor by consolidating bus stops.*


**7.	Add bus cut-outs** to prevent traffic obstruction at the Florida Avenue intersections with Orchard Street (on the SE Corner) and Vine Street (on the NW corner, at Blair Park). Cut-outs will include concrete landing pads and shelters for bus riders.	

**Support: 66%      Concern: 16%**

*Comments: Respondents shared concerns that adding bus cut-outs may encourage speeding and increase congestion as buses reenter the corridor. Others voiced concern that adding bus cut-outs could encourage cars to bypass the buses and speed along the corridor and that the placement of cut-outs could make backing out of driveways an issue.*

**8.	Add two-stage turn queue boxes** at the Florida Avenue intersection with Race Street to help cyclists transition between on-street bike lanes and off-street shared-use paths.	

**Support: 49%      Concern: 17%**

*Comments: Respondents shared concerns that adding two-stage turn queue boxes will create confusion amongst motorists and cyclists. Those who made additional comments of support expressed desire to make the corridor safer for cyclists through these proposed interventions.*

**9.	Add new traffic signals** (including crosswalk signals) at the Florida Avenue intersections with Race Street and Vine Street to improve safety and traffic flow.	

**Support: 54%      Concern: 28%**

*Comments: Of the additional comments, nearly 39% expressed concern over the proposal for new traffic signals at the Florida Avenue intersection with Race Street and Vine Street. Respondents shared concerns that adding signals may encourage speeding and increase accidents along the corridor, increase congestion during peak hours, make biking/walking more challenging, and/or change the character of the neighborhood. Those who made additional comments of support expressed desire to make the corridor safer for pedestrians and motorists. Nine respondents suggested a roundabout as an alternative to traffic signals.*

**10.	Add a parking lane** on the north side of Florida Avenue between Busey Avenue and Race Street (within the existing roadway width, no pavement expansion needed).	

**Support: 24%      Concern: 36%**

*Comments: Of the additional comments, nearly 42% expressed concern with implementing a parking lane on the North side of Florida Avenue between Busey Avenue and Race Street. Respondents shared concerns that a parking lane may result in increased congestion and impair motorist visibility of oncoming traffic and pedestrians. Those who made additional comments of support voiced desire for any intervention that can slow traffic along the corridor.*


![Chart showing respondents' support and concern for proposed improvements.](./support_concern.png)


