---
title: "Public Involvement Info"
draft: false
weight: 5
bannerHeading: "Get Involved!"
bannerText: >
  No one knows the community like those who live, work, and go to school here. Get involved in Florida Avenue Corridor Study to share your input! 
---


{{%sidebar title="" position="top" %}}

### Share Your Input!

{{<button href="https://www.research.net/r/florida2" size="large">}}Take Survey{{</button>}}

*¡Encuesta disponible en español!*

*Sondage disponible en français!*

*提供中文版调查问卷！*

**The survey will be open until May 11, 2002.**

*Thank you to everyone who took the first survey in 2021 - over __400__ survey
responses were collected!* 

{{%/sidebar%}}

The Champaign-Urbana Urbanized Area Transportation Study is evaluating one mile
of Florida Avenue in Urbana (from Lincoln Avenue to Vine Street) in partnership
with the City of Urbana, UIUC, and MTD. The goal of this study is to identify
ways to increase transportation safety and mobility in this high-traffic
corridor. Your input will inform the future vision for Florida Avenue and help
increase mobility in the Champaign-Urbana area. 

## April 2022: Public Input, Phase 2!

After documenting [existing
conditions](https://ccrpc.gitlab.io/florida-ave/existing-conditions/transportation/),
collecting [public
input](https://ccrpc.gitlab.io/florida-ave/public-involvement/survey-results/),
and [evaluating future
scenarios](https://ccrpc.gitlab.io/florida-ave/future-scenarios/scenario-evaluation/),
the study's working group is proposing changes that are discussed on the [Future
Recommendations](https://ccrpc.gitlab.io/florida-ave/future-scenarios/future-recommendations/)
page. After you have familiarized yourself with the future recommendations and
watched the summary video below, **please [fill out the
survey](https://www.research.net/r/florida2) to provide your feedback!** The
survey will be open until Wednesday May 11, 2022. 

### Watch the following video for a summary of the future recommendations:

<iframe width="560" height="315" src="https://www.youtube.com/embed/fTnG_lSH5vE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**If you are new to the corridor study website, the following video from 2021 provides a short overview of the project and website:**

<iframe width="560" height="315" src="https://www.youtube.com/embed/wCiXJpTPCTY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 

### Project Timeline

  {{<image src="Timeline3.svg"
  alt="Figure showing strategic corridor study schedule"
  attr="Draft corridor study schedule"
  position="full">}}

<iframe src="https://www.research.net/r/6LN36XR" title="" width="100%" height="300"></iframe>

**For questions or comments please contact:**

- Ashlee McLaughlin, Planning Manager; amclaughlin@ccrpc.org; (217) 819-4077
- Kazi Jahan, Planner II; kjahan@ccrpc.org
- JD McClanahan, Planner II; JMcClanahan@ccrpc.org


