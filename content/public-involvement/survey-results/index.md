---
title: "Public Outreach - Phase One Summary"
draft: false
weight: 10
---

## Introduction

The first round of feedback was conducted via a SurveyMonkey survey linked to in
the project website at [ccrpc.org/florida](https://ccrpc.org/florida). This
survey featured questions on respondent’s use and access to different
transportation modes, open ended questions about their likes, dislikes, and
other thoughts on the corridor, as well as optional questions about demographics
and location of residency. The online survey was open to the public from June
30, 2021 to July 31, 2021. 

The following methods were used to make community members aware of the website and survey:

- Posting on the CUUATS Facebook, Instagram, and Twitter
- Sending the social media posts, digital flyers, and info blurbs to community partners for them to distribute. Partners contacted include:
    - CCRPC Communications office
    - CUMTD
    - City of Urbana
    - Urbana Parks District
    - PACE
    - The SUNA and WUNA neighborhood groups
    - UIUC Facilities, UIUC University Housing
- Installing yard signs in the public right of way along the Florida Avenue corridor
- Hanging door hangers at all homes directly on the corridor
- Discussing the study with reporters from The News-Gazette and WICS
- Tabling in Orchard Downs during the annual Bike to Work Day

In addition to survey responses, direct correspondence with CUUATS staff about
the study (including emails, mail, and phone calls) was documented and
incorporated into the written feedback record (this was limited; non-survey
responses totaled less than 10 responses). The public outreach process resulted
in 434 documented responses from the online survey, emails, or other means.


## Respondent Transportation

Based on the question “How often do you use each of the following transportation
modes on Florida Avenue between Lincoln Avenue and Vine Street?”, private motor
vehicles were by far the largest transportation mode on Florida for the
respondents. More respondents (85%) drive on the corridor weekly than use any of
the other modes in an entire year. However, on an annual basis, around 70% of
respondents will walk along the corridor, half of respondents will bike, a third
take the bus, and a quarter take a rideshare. This was reinforced in the next
question, which asked if respondents owned or had reliable access to motor
vehicles, bikes, or bus passes. 95% had reliable access to a motor vehicle,
compared to 74% having a bike and 49% having a bus pass.

 <rpc-chart url="florida_use.csv"
  chart-title="Transportation Use on Florida Avenue"
  type="horizontalBar"
  stacked="true"
  x-max=100
  wrap=40
  y-label="Transportation Mode"
  x-label="Percent of Respondents"></rpc-chart>

   <rpc-chart url="transportation_access.csv"
  chart-title="Access to Transportation Mode Among Respondents"
  type="horizontalBar"
  wrap=30
  y-label="Transportation Mode"
  x-label="Percent of Respondents"></rpc-chart>

## Short Answer Response Summaries

For each short answer question, responses were sorted into seven broad categories—

- Roadway Function
- Major Intersections
    - Lincoln Avenue and Florida Avenue
    - Orchard Street and Florida Avenue
    - Race Street and Florida Avenue
    - Vine Street and Florida Avenue
- Public Transportation
- Bike and Pedestrian Facilities
- Neighborhood Character
- Maintenance and Physical Conditions
- Miscellaneous

In response to the question “What do you 𝑳𝑰𝑲𝑬 about the current
transportation conditions on Florida Avenue between Lincoln Avenue and Vine
Street?”, the most common theme was Neighborhood Character, which encompassed a
range of subjects including the aesthetic character of the neighborhood (both
buildings and landscaping), trees, parks, and the experience of residents of the
area. The next most common theme was Roadway Function, with comments indicating
the value of this east-west transportation corridor to residents. Although not
an official category, more than 70 people responded with “Nothing” or some other
variant to this required question, indicating the high levels of frustration
that many residents have with this corridor. Other than the Miscellaneous
category, the least common theme for this question was Maintenance and Physical
Conditions, which is reflected in the next question, where it was a common
complaint.

The next question was “What do you 𝑵𝑶𝑻 𝑳𝑰𝑲𝑬 about the current
transportation conditions on Florida Avenue between Lincoln Avenue and Vine
Street? What would you like to see change?”. The most common theme for this
question was Bike and Pedestrian Facilities, with more than half of respondents
mentioning the need for improved walking or biking paths. The next most common
was Maintenance and Physical Conditions, with nearly half of respondents noting
the poor physical conditions, either on the road surface itself or on sidewalks
or other infrastructure. Finally, around a third of residents commented on the
intersections within the corridor, taking issue with the current conditions of
(most frequently) the 4-way stops at Race and Vine, in addition to the lights at
Orchard and Lincoln. After the “Miscellaneous” theme, which is understandably
smaller, the least common themes here were Public Transportation, where many
complaints were actually about the roads being inadequate for buses, and
Neighborhood Character, where aside from some particular issues, respondents
continued to show the satisfaction with many aesthetic and quality of life
attributes of the corridor they expressed in the previous question.

For the question, “If you live near this section of Florida Avenue, what else do
you think we should know about you or your neighborhood?”, the most common theme
was again Bike and Pedestrian Facilities, followed (predictably) by Neighborhood
Character and Road Function.

The fourth question, “Please provide any other comments about this section of
Florida Avenue or the content on the project website.”, again featured Bike and
Pedestrian Facilities as the most common theme, followed by Miscellaneous, Road
Function, and Neighborhood Character.

## Content Summaries by Theme

With the general trends in the themes established, this section will provide
brief summaries of the most prevalent ideas communicated within each theme.

Comments were also sorted by key words and closely associated terms—road
segments (Lincoln to Race, Lincoln to Orchard, Orchard to Race, Race to Vine),
intersections (Vine, Orchard, Race, and Lincoln), as well as the following
general keywords—narrow/wide, bus, emergency, turn, bike, walk, speed,
congestion, tree, and roundabout. Within the larger themes, keywords relevant to
that theme will also be discussed.

### Road Function

Many of the positive responses to road function appreciate the simple fact that
Florida exists as an east-west corridor connecting Urbana, Champaign, and the
university—providing a route for many people’s daily commutes or other trips.
Respondents also noted that outside of specific times, traffic flows relatively
smoothly. However, the negative responses complained of congestion during peak
hours, which many believe is aggravated by the road’s narrow, 2 lane condition
east of Lincoln. This reduction to two lanes, which happens directly after the
Lincoln Avenue stop light, was identified as a particular point of conflict. The
narrowness of the road also contributes to complaints about turning radii at the
major intersections. The narrow road condition also presents a problem for
delivery vans or other uses which temporarily stop and which are difficult to
navigate around in Florida Avenue’s current condition. 

Respondents had a variety of suggestions for how traffic on Florida Avenue could
be addressed more effectively, including increasing the speed limit, adding a
middle lane and using signaling to change its direction depending on the time of
day, making Florida and Pennsylvania each one-way roads, and designating one of
these roads as an auto corridor and one as a bike/bus corridor. Residents of
neighboring streets mentioned the current use of their streets as a bypass to
avoid Florida Avenue traffic, and were wary any efforts that would further drive
traffic off of Florida and on to their streets. Some respondents encouraged the
redirection of east/west traffic, from Florida to Windsor Road, one mile south.
Traffic calming suggestions along Florida included speed bumps or tables, as
well as bump outs or chicanes, all with the goal of naturally decreasing driving
speeds. Respondents also encouraged the connection of Florida Avenue all the way
to High Cross Road, including connecting the bike and pedestrian infrastructure
to this network.

“Narrow” and “wide” were prevalent keywords (together appearing in 142
comments), indicating the stress that results from users sharing a confined
space with motor vehicles or other modes. However, some did note the traffic
calming benefits of the narrow road and its contribution to a more residential
feeling.

Speed was also a common keyword (seen 74 times), with some users appreciating
the speed moderating effects of the current condition (narrow lanes, rough
surface, frequent access, intersection controls). However, others were concerned
about the lack of speed limit enforcement in the area, believing the current
limit to be too high and often exceeded. This led many respondents to discourage
efforts that would allow faster driving through the corridor while encouraging
speed bumps or tables.

Another keyword within this theme was Turn (59 appearances). Some respondents
appreciated left turn lanes at the intersection, feeling that they encourage the
flow of traffic. However, there were a variety of concerns with turning
infrastructure. Respondents felt that the turn lanes at four-way stops
contributed to the confusion about right of way. Users felt that the turn lanes
on west-bound Florida at Vine and Race were confusing, with the required
straight and turn lanes flipping sides at these two intersections. People also
expressed concern about the length of turn lanes, stating that they are not long
enough and cars and buses will pull into oncoming traffic to access the turn
lane. Turn radius was also a concern (particularly for buses), with users
mentioning the need to back up from stop lines in order to let buses make turns,
but noting similar concerns even for private motor vehicles. Private motor
vehicle users particularly noted the right turns onto Florida at the four-way
stops as tight turns.

Comments about congestion (13 appearances) noted concerns that bike/pedestrian
or safety interventions would make traffic worse along the corridor, ensuring
that Florida Avenue traffic didn’t drive traffic onto neighboring roads,
encouragement of alternative intersection controls to allow more free flowing of
traffic, separating cyclist and automotive uses to allow better traffic flow,
and encouraging alternatives to private automobiles in general in order to
reduce daily traffic.

### Major Intersections

Comments varied along the four major intersections in this corridor. As
previously mentioned, the main concern at Lincoln was the lane merging
immediately after, in addition to comments about improving bike and pedestrian
infrastructure (path continuity, adequate signal length). At Orchard, many
respondents commented that the traffic light seemed unnecessary given the amount
of traffic at this intersection, and held up traffic on Florida, while cyclists
and pedestrians crossing Florida here expressed appreciation for the ability to
stop traffic and safely cross. Race and Florida saw similar comments due to
their similar layout as four-way stops, with more comments concerned about the
Race Street intersection. Respondents noted that the four-way stop with straight
and turn lanes in each direction led to confusion about right of way and who
should proceed next, contributing to overall congestion. The frequency of cars
not stopping for the stop signs was also noted, as a hazard both for
bikes/pedestrians and other drivers. Regarding how to address these issues,
there is significant support both for traffic lights and for roundabouts.
However, there were concerns for each. With a traffic light, some respondents
appreciated the speed calming quality of the current stop signs, and believed
that drivers who didn’t need to stop due to a green light would drive
unnecessarily fast through the corridor. Roundabout concerns stemmed from the
learning process of drivers encountering the first roundabout in the area, as
well as pedestrian concerns about drivers who are not expecting to have to come
to a full stop at the intersection.

Race Street (150 appearances) was one of the most common keywords, with
respondents noting the confusion resulting from a busy 4-way stop with
additional turn lanes, the hazards to alternate transportation mode users from
confused or inattentive drivers, the practice of rolling stops or ignoring the
stop signs, and the tight turn radii for motor vehicles generally and buses in
particular. Suggestions for changes included adding a traffic light or a
roundabout, although residents had concerns about these controls as well (for a
light, the high speeds that could result from not needing to slow down for a
green light, and for a roundabout, the confusion of residents getting used to it
and the bike/pedestrian hazard of cars that aren’t expecting to come to a full
stop). 

The Vine Intersection (75 appearances) saw many overlapping concerns to the
similar four-way stop at Race, exacerbated by the proximity to Blair Park and
the bike and pedestrian traffic associated with that.

At the Orchard intersection (74 appearances), some respondents were confused
about the traffic light at this relatively low-traffic intersection and felt
that it held up traffic at Florida Avenue and should be removed or relocated to
one of the existing four-way stops. However, other users who bike or walk
between Orchard Downs/the arboretum and the areas to the north felt that the
light was an important safety feature, providing designated safe times for users
to cross the street.

At the Lincoln intersection (69 appearances), users appreciated the narrowing of
lanes and lowering of speed limits at this area. However, the eastbound merge
immediately after the light at Lincoln was frequently identified as a problem,
causing confusion to drivers and dangerous speeding in order to merge after
traffic gathered at the light. In addition to vehicle traffic merging, the
shared use path on the south side of Kirby/Florida ends at this intersection,
providing a stressful experience for all users. Respondents also expressed the
need for better signaling at this intersection, including the pedestrian signal
length not being sufficient for the width of Lincoln Ave.

As previously mentioned, respondent sentiment on roundabouts (24 appearances)
varied, with many strong advocates, especially at the Race and Vine
intersections, but also concerns about drivers getting used to the first
roundabout and the safety of pedestrians and cyclists in roundabout
intersections.

### Public Transportation

Bus and MTD together had 66 keyword appearances. Most of the complaints or
concerns were not related to the bus service itself, which riders appreciated,
but to the relationship between the buses and the built infrastructure along the
corridor. Most commonly, other drivers expressed concerns about lane width and
turn radii, noting the needs of buses to pass into other lanes while turning in
order to navigate their routes along Florida Avenue.  Some respondents
identified heavy buses as a likely contributor to road wear and tear. With the
narrow road width, other respondents identified bus stops as contributing to
overall traffic congestion. Lastly, bus riders did request better stop
infrastructure, noting that several stops along the corridor drop passengers
onto unsheltered and unpaved areas, posing a particular problem in inclement
weather.

### Bike and Pedestrian Facilities

Bike and pedestrian infrastructure was a theme which received a high quantity of
responses (in the keyword search, walk/pedestrian appeared 335 times, and
bike/cycle appeared 320), reflecting both extensive use of these modes in the
corridor and room for improvement. Positive feedback included the safe
signalized crossings, the segments with bike lanes and sidewalk on both sides of
the road, and the prospect of a shared-used path. The most frequent negative
feedback included the current lack of a path along Orchard Downs, the narrow
lanes and poor road maintenance making biking in the road hazardous
(particularly in the Lincoln to Race section), and lacking maintenance of
sidewalks affecting safety and accessibility. Maintenance complaints for both
cyclists and pedestrians included the road or sidewalk surfaces themselves, as
well as failure to clear of debris accumulation on the sidewalk or along the
shoulder/bike lane. In addition, respondents expressed a desire for more
sidewalks to safely cross Florida, and a sense of stress when crossing the 4-way
stops, due to the hectic conditions discussed in previous themes. Several
respondents mentioned answering “Never” to the question about biking in the
corridor, not because they don’t bike in the neighborhood, but because they go
out of their way to avoid Florida Avenue due to the perceived danger of biking
here.

### Neighborhood Character

In this theme, both residents and commuters expressed an appreciation for the
aesthetic character of the corridor, including the tree canopy, the homes
themselves, and the landscape (both private yards and the open space of Orchard
Downs). Residents expressed a desire for any improvements not to impede on
private property and compromise yards or homes, preferring to utilize the
university open space on the south side of Florida. Residents did express
several quality of life concerns from living on the corridor, including the lack
of street parking, the stress of exiting their driveways during rush hour, and
the noise and exhaust from private motor vehicles, buses, and emergency
vehicles. Some residents also mentioned the use of the corridor by commercial
shipping trucks, which they believe should be redirected. Residents commonly
expressed the opinion that Florida Avenue is a residential street, and that that
purpose should be prioritized above through traffic. This was also expressed
through the belief that Florida currently serves as a barrier or a boundary in
the neighborhood, and should be made to better facilitate movement and
interaction between residents on both sides of it. Respondents also expressed a
desire for connection and continuity along the length of the corridor, including
consistent street lighting. In addition to objecting to the city’s decision to
remove street parking, several residents mentioned the removal of the median at
the Lincoln intersection as a point of contention. One respondent noted the
feeling of danger mowing his lawn due to the lack of road shoulder, emphasizing
the proximity of homes to traffic. Overlapping with the bike and pedestrian
theme, several respondents noted the desire for safety for their whole family on
the corridor, including their children who play outside, walk, or bike in the
neighborhood. Several respondents also noted the importance of Orchard Downs in
providing affordable student housing near campus, and expressed their desire for
it to continue to serve this purpose and be safely connected to the main area of
campus, particularly by bus. The Urbana Parks District is frequently invoked by
residents voicing their appreciation for Blair Park and the district’s work to
add additional paths along all four sides of the park. However, many expressed a
desire for safer pedestrian access to the park from neighboring streets
(particularly from the Florida/Vine intersection).

More than 60 respondents provided some variation of “nothing” as their answer to
the required “what do you like” question, indicating high levels of frustration
with the current state of the corridor. On the other hand, nearly a dozen
provided this answer to the “what don’t you like” question, indicating the
desire of some residents to maintain the corridor as is.

Parking appeared 30 times as a keyword. Many residents expressed frustration
with the city’s choice to remove street parking west of Race Street in the last
decade, coupled with small driveways for their homes. However, in light of
current traffic levels, some respondents appreciated the lack of parking,
believing that it would add an additional safety hazard to road users.

Emergency/Ambulance/sirens together appeared 11 times in the comments, with
residents noting the noise from emergency vehicles along the corridor.

Trees appeared 76 times in the keyword search. The existing tree canopy was a
very commonly appreciated asset for both residents of the corridor and travelers
along it. However, concerns were expressed about maintenance issues related to
trees, such as foliage blocking the view from some intersections, as well as
sidewalk issues related to foliage and roots.

### Maintenance and Physical Conditions

Maintenance of road and sidewalk surfaces was an extremely frequent source of
complaints. Some respondents provided qualified positive responses to the road
surface, stating that it was not as bad as other roads in the area. Others noted
that the road surface serves as an unintentionally traffic calming mechanism.
However, most respondents took issue with the quality of the road surface,
noting the prevalence of potholes that serve as safety hazards (particularly for
cyclists), repair costs for drivers due to daily use of the corridor, and
generally unpleasant experience. As previously mentioned, sidewalk surface
conditions were also noted as poor by many respondents, but complaints about
road surface significantly outnumbered these. 

### Miscellaneous

Miscellaneous comments included complaints about the behavior of drivers and
cyclists along this corridor, suggestions for other studies or road improvements
(Anderson Ave, Maine Street, Springfield), and statements of appreciation for
the public outreach project and the effort to improve this corridor.


