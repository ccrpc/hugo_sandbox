---
title: "Public Outreach - Phase 1 Respondent Demographics"
draft: false
weight: 20
---

# Introduction

The first round of feedback was conducted via a SurveyMonkey survey linked to in the project website at [ccrpc.org/florida](https://ccrpc.org/florida). This survey featured questions on respondent’s use and access to different transportation modes, open ended questions about their likes, dislikes, and other thoughts on the corridor, as well as optional questions about demographics and location of residency. The online survey was open to the public from June 30, 2021 to July 31, 2021. 

The following methods were used to make community members aware of the website and survey:

- Posting on the CUUATS Facebook, Instagram, and Twitter
- Sending the social media posts, digital flyers, and info blurbs to community partners for them to distribute. Partners contacted include:
    - CCRPC Communications office
    - CUMTD
    - City of Urbana
    - Urbana Parks District
    - PACE
    - The SUNA and WUNA neighborhood groups
    - UIUC Facilities, UIUC University Housing
- Installing yard signs in the public right of way along the Florida Avenue corridor
- Hanging door hangers at all homes directly on the corridor
- Discussing the study with reporters from The News-Gazette and WICS
- Tabling in Orchard Downs during the annual Bike to Work Day

In addition to survey responses, direct correspondence with CUUATS staff about the study (including emails, mail, and phone calls) was documented and incorporated into the written feedback record (this was limited; non-survey responses totaled less than 10 responses). The public outreach process resulted in 434 documented responses from the online survey, emails, or other means.

# Area of Residence

Out of 427 respondents to the survey itself, 390 provided the nearest intersection to their home, as seen in the maps and table below.

<rpc-table url="response_distribution.csv"
  table-title="Survey Respondents by Location"
  source-url="https://ccrpc.org/"
  text-alignment="l,c"></rpc-table>

{{<image src="map_response_all.jpg"
  alt="Map showing one dot for each surve response, across entire urban area"
  caption="All survey responses in the C-U area"
  position="right">}}

  {{<image src="map_response_zoom.jpg"
  alt="Map showing one dot for each surve response, zoomed in to show Florida Avenue corridor, along with the Area of Influence and a half-mile buffer"
  caption="Survey responses nearest to the FLorida Avenue Corridor"
  position="right">}}





This shows that responses came from across the Champaign-Urbana urbanized area, with the largest concentration in southwest Urbana around the corridor itself. 19% of respondents live in the study area, 34% within the corridor area of influence, and 62% within a half-mile of the study area. This distribution provides a useful variety of perspectives from respondents who primarily use the corridor for transportation and those who live or recreate in the area, so that we can understand the needs of a variety to user types.

According to the 2010 Census, 2,260 people lived in the blocks adjacent to the Study Area, and 4,345 lived in the blocks in the Area of Influence. As mentioned in the Transportation section of Existing Conditions, the most recent traffic count data from 2016 found an average daily traffic of 10,300 for the Lincoln to Orchard segment, 9,300 for Orchard to Race, and 8,800 for Race to Vine. Comparing these resident and traffic counts, we see that although the residents of the corridor are deeply affected by the road and should be considered in any proposals, they are a fraction of the overall users, and their perspectives must be considered alongside the context of other community needs. 


# Respondent Demographics

### Age

The greatest proportion of responses came from residents above the age of 40, with this cohort representing 70% of survey respondents. Comparing this to the population demographics of the Area of Influence and Champaign County, we can see that the 40-59 and 60-79 cohorts were significantly overcounted, and the 80+ cohort was slightly overcounted when compared to the area of influence but undercounted when compared to the county at large. In contrast, the 20-39 cohort was undercounted by a fairly substantial margin, and the under 20 cohort saw 1/40th of its representation in the area of influence. These statistics could be due to a variety of factors, including student residents being gone for the summer and the difficulty of engaging student renters vs long-term residents. Although efforts were made to engage with student populations in this first round of feedback, this group will be the focus of increased attention in future public engagement efforts, in order to ensure a representative perspective from the populations affected by this corridor.


 <rpc-chart url="age.csv"
  chart-title="Age of Respondents"
  type="horizontalBar"
  y-label="Age"
  x-label="Percent of Respondents"></rpc-chart>
 
### Gender

Slightly more than half of the respondents identified as female (52%), slightly more than 40% as male, with the remaining preferring not to answer or self-describing as other.

<rpc-chart url="gender.csv"
  chart-title="Gender of Respondents"
  type="horizontalBar"
  y-label="Employment Status"
  x-label="Percent of Respondents"
  x-min="0"
  x-max="100"></rpc-chart>
 
### Race and Ethnicity

Non-Hispanic white residents comprised the vast majority of respondents, with 91% of responses from this group. The next most prevalent was Asian respondents, at 7%. All other races, as well as Hispanic/Latino ethnicity, made up 2% or less each. Compared to local ACS data, we can see that white residents are significantly overcounted, as they make up 65% of AoI residents and 72% of county residents. Much of this undercounting is likely due to the previously mentioned difficulty in engaging students in taking the survey and will be a focus of attention in future outreach efforts. 

<rpc-chart url="race.csv"
  chart-title="Race and Ethnicity of Respondents"
  type="horizontalBar"
  y-label="Race or Ethnicity"
  x-label="Percent of Respondents"></rpc-chart>
 
### Employment

Nearly two thirds of respondents work outside the home, 20% are retired, 19% work inside the home, and 8% are students, and 4% or less each are homemakers, other, or looking for work.

<rpc-chart url="work.csv"
  chart-title="Employment Status of Respondents"
  type="horizontalBar"
  y-label="Employment Status"
  x-label="Percent of Respondents"
  x-min="0"
  x-max="100"></rpc-chart>




