---
title: "Scenario Evaluation"
draft: false
weight: 20
---

## Travel Forecasting

The [Champaign County Travel Demand Model
(TDM)](https://ccrpc.gitlab.io/lrtp2045/data/tdm/) is a transportation planning
tool developed to evaluate the existing transportation system and forecast
future travel demand in the region. The model uses the roadway network,
transit network, socio-economic data, land use data, and existing travel
patterns to estimate current traffic volumes in relation to roadway capacity.
The model also predicts future travel patterns, based on changes to the roadway
network, in combination with socio-economic, land-use, and other data. 

The TDM is integrated with [UrbanSim’s land use
model](https://ccrpc.gitlab.io/lrtp2045/data/models/#land-use-model-urbansim) to
identify the relationships between population, employment, land use, and travel
changes in the region over a 30 year period. The land use model relies on
regionally-approved population and employment projections developed for the
[Long Range Transportation Plan 2045](https://ccrpc.gitlab.io/lrtp2045/).
Together, the TDM and land use models forecast future traffic volumes on the
study area road network. 

By 2045, these models forecast traffic volume increases of 19-29
percent along the studied portion of Florida Avenue, with the highest
increase between Lincoln Avenue and Orchard Street.

<iframe src="https://maps.ccrpc.org/facs-future-adt-change/"
  width="100%" height="400" allowfullscreen="true">
  Map showing percentage changes in the Average Daily Traffic between 2015 and 2045 in the Florida Avenue Corridor Study Area of Influence.
</iframe>

## Future Scenarios
### Baseline Scenario
To understand the future mobility needs of the corridor, staff established a
Baseline Scenario, calculating future roadway Level of Service (LOS) and traffic
delays based on the future traffic volumes forecasted for 2045. The Baseline
Scenario is based on the [LRTP 2045 Preferred
Scenario](https://ccrpc.gitlab.io/lrtp2045/vision/model/), which includes
bicycle and pedestrian improvements planned for implementation through 2045, but
no roadway improvements in the study area. Because of this, the Baseline
Scenario can be used as a control, against which other scenarios with proposed
roadway improvements can be compared. The scenario evaluation process described
below compares intersection treatments at the four major intersections along the
Florida Avenue Corridor—Lincoln Avenue, Orchard Street, Race Street, and Vine
Street. This process can provide guidance for improving vehicle level of service
as well as safety, which impacts all modes using the corridor. Additional
details regarding other transportation improvements for pedestrians, cyclists,
and transit vehicles are outlined on the [Future Recommendations
page](https://ccrpc.gitlab.io/florida-ave/future-scenarios/future-recommendations/).

#### Operational Conditions of Selected Intersections
Future traffic conditions were evaluated for the four busiest intersections
along the Florida Avenue corridor—located at Lincoln Avenue, Orchard Street,
Race Street, and Vine Street. The same intersection measurements used to
evaluate the [existing intersection
operations](https://ccrpc.gitlab.io/florida-ave/existing-conditions/transportation/#operational-conditions-of-selected-intersections)
(approach delay, intersection delay, and level of service (LOS)) were used to
evaluate the future traffic operations during the morning and afternoon peak
hours on a typical weekday. The full table summarizing future intersection
operations for the Baseline Scenario can be found [in the
appendix](https://ccrpc.gitlab.io/florida-ave/appendices/#intersections-los-for-morning-and-afternoon-peak-hours-20172018-v-2045-baseline-scenario).

Looking at these four intersections during the morning and afternoon peak
traffic hours, the following figures compare the combined LOS for 2017 and 2018
with the forecasted LOS for the 2045 Baseline Scenario. In 2045, three of the
four intersections have approaches where the operating conditions are expected
to reach high levels of congestion and delay (LOS E or F). Because of this,
these intersections will require attention to improve their operational
conditions.

#### Intersections LOS for Morning Peak Hours, 2017 &amp; 2018 v. 2045 Baseline Scenario

{{<image src="LOS_Intersection_2045_AM.svg"
  alt="Intersections Level of Service for Morning Peak Hours for 2045 Baseline Scenario along Florida Avenue"
  caption= ""
  position="full">}}
  See [appendices](https://ccrpc.gitlab.io/florida-ave/appendices/#intersections-los-for-morning-and-afternoon-peak-hours-20172018-v-2045-baseline-scenario) for more information.

#### Intersections LOS for Afternoon Peak Hours, 2017 &amp; 2018 v. 2045 Baseline Scenario

{{<image src="LOS_Intersection_2045_PM.svg"
  alt="Intersections Level of Service for Afternoon Peak Hours for 2045 Baseline Scenario along Florida Avenue"
  caption= ""
  position="full">}}
  See [appendices](https://ccrpc.gitlab.io/florida-ave/appendices/#intersections-los-for-morning-and-afternoon-peak-hours-20172018-v-2045-baseline-scenario) for more information.

#### Operational Conditions of Roadway Segments

The Travel Demand Model was also used to analyze the future operational
conditions of this corridor’s individual roadway segments (Lincoln to Orchard,
Orchard to Race, and Race to Vine). Travel time was calculated for each segment
using [existing speed
data](https://ccrpc.gitlab.io/florida-ave/existing-conditions/transportation/#speed-study),
speed forecasts from the [LRTP 2045 Preferred
Scenario](https://ccrpc.gitlab.io/lrtp2045/vision/model/), and approach delay
for the downstream intersections. Future LOS was calculated for each segment
using travel speed as a percentage of base free-flow speed. The full table
summarizing future segment operations forecasted for the Baseline Scenario can
be found [in the
appendix](https://ccrpc.gitlab.io/florida-ave/appendices/#segments-los-for-morning-and-afternoon-peak-hours-2016-v-2045-baseline-scenario).


Compared with the [existing
conditions](https://ccrpc.gitlab.io/florida-ave/existing-conditions/transportation/#operational-conditions-of-roadway-segments),
worsening conditions are expected for all three segments, both eastbound and
westbound, during both the morning and afternoon peak travel hours. The
following figures illustrate the projected changes. The most dramatic morning
decline is from LOS B in 2016 to LOS F in 2045 for the westbound segment of Vine
Street to Race Street, while the largest afternoon decline is from LOS B to LOS
F for the eastbound segment of Orchard Street to Race Street. 

#### Segments LOS for Morning Peak Hours, 2016 v. 2045 Baseline Scenario

{{<image src="LOS_Segment_2045_AM.svg"
  alt="Segments Level of Service for Morning Peak Hours for 2045 Baseline Scenario along Florida Avenue"
  caption= ""
  position="full">}}
  See [appendices](https://ccrpc.gitlab.io/florida-ave/appendices/#segments-los-for-morning-and-afternoon-peak-hours-2016-v-2045-baseline-scenario) for more information.

#### Segments LOS for Afternoon Peak Hours, 2016 v. 2045 Baseline Scenario

{{<image src="LOS_Segment_2045_PM.svg"
  alt="Segments Level of Service for Afternoon Peak Hours for 2045 Baseline Scenario along Florida Avenue"
  caption= ""
  position="full">}}
  See [appendices](https://ccrpc.gitlab.io/florida-ave/appendices/#segments-los-for-morning-and-afternoon-peak-hours-2016-v-2045-baseline-scenario) for more information.

### Additional Scenarios
Considering the results of the 2045 baseline/no build scenario and the public
input received during the first round of input collected in 2021, the Florida
Avenue Corridor Study Working Group (made up of representatives from the City of
Urbana, University of Illinois, MTD) developed four additional scenarios,
evaluating different intersection control techniques to improve the operational
conditions throughout the corridor for current and future traffic volumes. These
scenarios all evaluated the four major intersections with Florida Avenue along
this corridor—at Lincoln Avenue, Orchard Street, Race Street, and Vine Street.
Due to the limitations of the existing four-way stop-controlled intersections,
all of the considered additional scenarios involved some combination of signals
and roundabouts.

-	2045 Scenario 1: Maintaining the signals at the intersections with Lincoln and Orchard, while also converting Race and Vine to traffic signals (with two different options for how those signals would be coordinated with each other)
-	2045 Scenario 2: Converting the four major intersections in the corridor to roundabouts 
-	2045 Scenario 3: Maintaining Lincoln’s signal and converting the other three major intersections to roundabouts
-	2045 Scenario 4: Keeping the signal at Lincoln and Orchard while changing Race and Vine to roundabouts 

The image below shows the results produced from the operational analysis (using
the Synchro 10 software suite): the intersection level of service during the
morning and afternoon peak hours for all 6 scenarios evaluated (existing
conditions, 2045 baseline/no-build, and the four additional 2045 scenarios). The
intersection level of service from the 2045 baseline/no build scenario can be
compared with 2045 Scenarios 1 through 4 to evaluate which combination of
traffic signals and roundabouts would most improve operational conditions.
Definitions for each of level of service can be found
[here](https://ccrpc.gitlab.io/florida-ave/existing-conditions/transportation/#traffic-operations);
A is the best level of service, F is the worst, and A-D are generally considered
acceptable levels, while E and F are unacceptable.

{{<image src="LOS_All_Scenarios_2045.jpg"
  alt="Intersections Level of Service for Morning and Afternoon Peak Hours for 2045 Scenarios along Florida Avenue"
  caption= "Performance of Considered Intersection Treatments"
  position="full">}}

As can be seen from the chart, all four of the possible changes to the
corridor’s intersections resulted in better performances than keeping the status
quo. In fact, the four possible changes would allow for better than current
performance in 2045, even given several decades of population growth and
development in the region. Given the acceptable results of all four of these
future scenarios, the working group considered additional factors to select the
best combination of signals and roundabouts.

See [appendices](https://ccrpc.gitlab.io/florida-ave/appendices/#2045_Scenario1_Coordinated) for more information.

### Scenario Selection
While all four future scenarios produced acceptable results, the biggest
question for the working group to consider was the feasibility of installing the
city’s first roundabouts in this high-traffic corridor. When considering the
prospect of installing roundabouts, the corridor study Working Group first
considered the existing right of way throughout the corridor as well as the
additional space that roundabouts may require. This stretch of Florida Avenue is
characterized by a fairly narrow right of way, along with homes with modest
setbacks from the street. This would mean that acquiring the space for a
roundabout of the proper radius at these intersections would involve purchasing
the needed land from property owners, and in some cases, purchasing the entire
lot and demolishing the home. The disruption to the neighborhood residents and
the cost to the City of Urbana from this acquisition would be substantial. The
Working Group was not convinced that the potential benefits of roundabouts at
these locations would outweigh the impact on the neighborhood and cost to the
City. 

In addition to right-of-way costs and neighborhood impact, this corridor would
not be the ideal location for Champaign-Urbana's first roundabout on an arterial
roadway for several additional reasons. Florida Avenue is a fairly significant
bus corridor, and if the limited space available restricted the
circumference of roundabouts at different intersections, this would impact the way that
MTD buses are able to navigate the roundabouts. One common way to constrain a
roundabout's overall circumference is to make the central island smaller and
traversable by MTD buses (and other large vehicles) in order to accommodate the
larger turning radius required by the buses. This option was considered at the
intersection of Florida Avenue and Orchard Street due to the lower traffic
volumes at the north and south approaches. While traversable central islands are
possible and recommended in some cases, they can be less effective in enforcing
the design intent of roundabouts. If MTD buses consistently navigate the
roundabout in a way that is not allowed for other vehicles, by traversing across
the central island rather than circumnavigating it, this could confuse drivers
who aren’t familiar with how to properly navigate roundabouts. It could also
convince other drivers that all vehicles can travel through the center island,
which would create unsafe conditions.

The first major roundabout in Champaign-Urbana should be easy to navigate for
all modes in a consistent manner. The constraints along the Florida Avenue
corridor prevent this from being the case. The Working Group remains in support
of the exploration and construction of roundabouts in the Champaign-Urbana
community; however, the limitations present on the Florida Avenue Corridor were
deemed too great to make roundabouts the preferred approach here.

#### Preferred Scenario: Scenario 1 - Signalizing the Four Major Intersections

Taken together, these concerns about roundabout implementation in the corridor
resulted in the Working Group **recommending Scenario 1.** Scenario 1 includes
upgrading the existing traffic signals at the Florida Avenue intersections with
Lincoln Avenue and Orchard Street to improve functionality. This scenario also
includes installing new traffic signals at the Florida Avenue intersections of
Race Street and Vine Street to improve safety and traffic flow. All four of
these new traffic signals should also include accessible audible pedestrian
signals and push-button devices, allow for vehicle and bicycle detection, and
provide an opportunity to implement Traffic Signal Priority and Preemption to
give priority to emergency vehicles and public transit buses when necessary.

The Working Group is confident that Scenario 1 will significantly improve the
corridor’s level of service above the current status quo, while avoiding some of
the logistical issues and costs associated with roundabouts. However, despite
traffic signals requiring less space than roundabouts, it is still possible that
some additional right-of-way will need to be acquired from the University or
other adjacent property owners to install new signals or properly update other
transportation facilities along the corridor. A full list of recommended
improvements is included on the [Future Recommendations
page](https://ccrpc.gitlab.io/florida-ave/future-scenarios/future-recommendations/).
