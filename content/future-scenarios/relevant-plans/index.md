---
title: "Relevant Plans"
draft: false
weight: 10
---

This section provides a summary of transportation projects already scheduled or
recommended within the Florida Avenue Corridor Study area. These plans and
priorities are taken from current transportation plans in the region, and the
decision to prioritize these particular projects came from input from members of
the public, as well as collaboration between local agencies. 

### Transportation Improvement Program (TIP), Fiscal Years 2020-2025

The [Transportation Improvement Program
(TIP)](https://ccrpc.org/documents/tip-2020-2025/) is a federally required
document which lists local transportation projects prioritized for construction
during the next four to six years. The [TIP Project
Database](https://maps.ccrpc.org/tip-2020-2025/) reflects transportation
projects throughout the Metropolitan Planning Area that are scheduled for
implementation and have relatively secure funding. The [TIP Illustrative
Projects Table](https://ccrpc.org/data/tip-illustrative-fy20-25/) reflects
transportation projects that have been identified as priorities but do not yet
have funding.

#### TIP FY 2020–2025 Projects in the Florida Avenue Corridor Study Area

The City of Urbana has made it a priority to improve the safety and function of
the Florida Avenue corridor by securing funding to reconstruct/rehabilitate
Florida Avenue between Lincoln Avenue and Vine Street. Because of its
consistency with federal and regional transportation goals, this project has
been approved to utilize federal Surface Transportation Block Grant (STBG)
funding, in addition to Urbana Motor Fuel Tax funds in fiscal year 2024. The
goal of this Florida Avenue Corridor Study is to define what future improvements
should be implemented, using alternative analysis and public involvement for
guidance. Both the corridor study (TIP Project ID: RPC-21-03) and the future
infrastructure improvements (TIP Project ID: UR-23-06) are reflected as funded
projects in the [TIP Project Database](https://maps.ccrpc.org/tip-2020-2025/).

Adjacent to the Florida Avenue Corridor Study area, Lincoln Avenue (from Florida
Avenue to Green Street), Pennsylvania Avenue (from Lincoln Avenue to Dorner
Drive), and Dorner Drive (from Pennsylvania Avenue to Gregory Drive) have also
been prioritized for transportation improvements by the City of Urbana and
University of Illinois. The improvements proposed for these locations are outlined
in the TIP’s [Illustrative Projects
Table](https://ccrpc.org/data/tip-illustrative-fy20-25/) and do not currently
have funding.

### Urbana Pedestrian Master Plan, 2020

The [2020 Urbana Pedestrian Master Plan
(UPMP)](https://ccrpc.org/documents/2020-urbana-pedestrian-master-plan-final-report/)
is a guide to help the City of Urbana plan for infrastructure and programs to
create a more walkable community. The plan sets goals and objectives to address
accessibility and connectivity, equity, safety, and vibrancy. CCRPC conducted
eleven public workshops during the UPMP planning process. 

#### UPMP Public Input Related to the Florida Avenue Corridor Study

- In the first round of public input, people noted that they avoid walking along
  this section of Florida Avenue due to poor pavement/paths, lack of crossing
  points, noise pollution, fear of crime, inadequate crossing times, and long
  waiting times to cross at the intersection at Florida Avenue and Vine Street.
- In the second round of public input, people voted on locations they wanted the
  City of Urbana to target pedestrian infrastructure improvements. Votes
  received for segments of Florida Avenue in the corridor study area are listed in the tables below.

##### UPMP Public Votes for Pedestrian Improvements in Florida Avenue Corridor Study Area, 2018

<rpc-table
url="UPMPsegments.csv"
source=" Urbana Pedestrian Master Plan, 2020"
source-url="https://ccrpc.org/documents/2020-urbana-pedestrian-master-plan-final-report/">
</rpc-table>

<rpc-table
url="UPMPintersections.csv"
source=" Urbana Pedestrian Master Plan, 2020"
source-url="https://ccrpc.org/documents/2020-urbana-pedestrian-master-plan-final-report/">
</rpc-table>

### Long Range Transportation Plan (LRTP 2045), 2019
  
The [Long Range Transportation Plan (LRTP)](https://ccrpc.gitlab.io/lrtp2045/)
is a federally mandated document that is updated every five years. This document
looks at the projected evolution of pedestrian, bicycle, transit, automobile,
rail, and air travel in the region over the next 25 years. CCRPC completed the most recent
update of the region’s LRTP (for the horizon year 2045) in 2019. The LRTP 2045
planning process provided multiple opportunities for public involvement. Input
relevant to the Florida Avenue Corridor Study is presented below.  

#### LRTP 2045 Public Input Related to the Florida Avenue Corridor Study

Out of seven overarching trends expected to influence transportation in
our community in the next 25 years, residents selected [*Walking and Biking for
Health*](https://ccrpc.gitlab.io/lrtp2045/process/round-one/#question-four) more
often than any other. 

{{<image src="LRTPvoices.JPG"
  alt="screenshot of LRTP online input map, C-U Transportation Voices, https://ccrpc.gitlab.io/transportation-voices-2045/"
  position="right">}}

The [LRTP 2045 online map](https://ccrpc.gitlab.io/transportation-voices-2045/)
documents the following public comments on Florida Avenue in the study area: 

- Florida Avenue between Orchard Street and Carle Avenue
    - Walking or Wheelchair comment: Increase separation between motorists and
      pedestrians: “A multiuse path here would be important for connectivity and
      safety (peds and bikes), sidewalk on north side has many obstacles.” (2
      likes)
    - Bicycle comment: “Add an off-street sidepath or trail” (3 likes)
- Intersection of Florida Avenue and Race Street 
    - Bicycle comment: Add an on-street bike lane: “Bike path just ends! Have to
      bike on dangerous roads. I know many people who have gotten hit by cars on
      the streets in this area.”
    - Automobile comment: “Weakness: 4 way stops at Florida Avenue and Race
      Street. Needs a light or roundabout.”
- Intersection of Florida Avenue and Vine Street
    - Bicycle comment: “This section of Florida Avenue is in very high need to
      being repaired for both cars and bikes alike. This area is very rough
      around the intersection of Florida Avenue and Vine Street.”
    - Bicycle comment: Add an on-street bike lane: “The cars parked on the
      street make it difficult to share the road.”
    - Automobile comment: Fix uneven pavement
    - Automobile comment: “Convert intersection to a roundabout” (2 likes)

### Champaign-Urbana Pedestrian and Bicycle Survey (PABS), 2019

The [Champaign-Urbana Pedestrian and Bicycle Survey
(PABS)](https://ccrpc.gitlab.io/pabs/) was administered in 2019 by the
Champaign-Urbana Urbanized Area Transportation Study (CUUATS). This survey was
carried out as part of a larger grant to expand local efforts collecting and
monitoring active transportation activity and infrastructure in the region. A total
of 1,631 questionnaires were mailed to households in the Champaign-Urbana metropolitan planning 
area in 2019, and of these, 550 questionnaires were completed.

#### PABS 2019 Responses Related to the Florida Avenue Corridor Study

Two [2019 PABS questions](https://ccrpc.gitlab.io/pabs/responses/) asked
respondents to list their favorite and least favorite locations in the community
to walk and bike. Florida Avenue from Lincoln Avenue to Race Street was listed
as a least favorite location to walk, and Florida Avenue between Lincoln Avenue
and Vine Street was listed as a least favorite location to bike. No respondents
listed Florida Avenue between Lincoln Avenue and Vine Street as a favorite
location to walk or bike. 

### The Impact of Place: University of Illinois at Urbana-Champaign Campus Master Plan, Updated: August 2018

The stated purpose of the [University of Illinois at Urbana-Champaign Campus
Master Plan](
https://www.uocpres.uillinois.edu/UserFiles/Servers/Server_7758/file/UIUC/mastrpln/uiucmp-tech-rpt-20180828.pdf)
is “first, to protect and celebrate the legacy of the University of Illinois at
Urbana-Champaign, in its history, its stature, and its sense of place; second,
to look forward and provide a framework to guide campus growth, set collective
priorities, and manage future investment.” One component of this purpose is to
analyze the existing transportation infrastructure in and around campus, as well
as planning for how this infrastructure can best serve the future needs of the
campus community while meeting goals of sustainability and safety. In analyzing
current conditions and planning for the future, the campus plan takes note of
several issues of interest, both along the Florida Avenue Corridor and within
its area of influence.

#### Campus Master Plan Findings Related to the Florida Avenue Corridor Study

##### Facilities and Campus Analysis

Within the existing Facilities and Campus Analysis chapter of the plan, the
[Circulation and Transportation](
https://www.uocpres.uillinois.edu/UserFiles/Servers/Server_7758/file/UIUC/mastrpln/uiucmp-tech-rpt-20180828.pdf#page=106)
section discusses the current condition of road, bike, pedestrian, transit, and
parking infrastructure on or adjacent to campus. Florida Avenue is identified as
one of the highest volume streets around campus, with over 20,000 average daily
traffic (ADT), while Lincoln Avenue is recognized as a secondary thoroughfare
with between 12,000 and 20,000 ADT.

In the analysis of pedestrian infrastructure, the Florida Avenue corridor is not
listed as an area of highest priority; however, the intersection of Florida and
Lincoln is noted as an area of conflict identified by university staff and
community members. In their assessment of bicycle infrastructure around the
university, Florida Avenue was named as an area of concern, due to its role
connecting the main campus to Orchard Downs and the City of Urbana and the
current gap in cycling infrastructure between Lincoln and Race. Lincoln Avenue
was also recognized as an area of concern, due to its similar lack of
infrastructure and the frequency of auto/bicycle accidents on this road.

{{<image src="campus_bike_map.jpg"
  alt="Map from campus plan showing existing and proposed bike infrastructure"
  position="right">}}

Lastly, the analysis of current conditions includes identification of the major
gateways into campus. This section named the corner of Florida and Lincoln as a
gateway that “needs to be reconsidered, redeveloped”, based on “the visual
quality and landscape features of the space”, in order to provide a more
effective entry onto campus for community members and visitors.

##### Master Plan

Following the analysis of current conditions, the [Master Plan](
https://www.uocpres.uillinois.edu/UserFiles/Servers/Server_7758/file/UIUC/mastrpln/uiucmp-tech-rpt-20180828.pdf#page=127)
chapter provides several recommendations for future action within the Florida
Avenue Corridor and area of influence. 

The plan calls for improvements to the bicycle infrastructure along Lincoln
Avenue as a particularly pressing need, and calls for both Lincoln Avenue and
Florida Avenue (from Lincoln to Race) to be part of a future bicycle route,
filling in infrastructure gaps and connecting to existing bicycle routes
elsewhere along the corridor.

Roadway changes proposed in the plan include work within Orchard Downs,
improving Orchard Drive and rerouting other traffic within the complex through
construction of new roads and closing some existing roads. This would include
re-opening Carle Avenue within Orchard Downs and connecting it to the occupied
portions of the complex. In addition, the plan calls for improvements on St
Mary’s Road (the southwest corner of the area of influence) to allow for
increased traffic flow. Lastly, this section calls for the “extension and
improvement of South Lincoln Avenue and Race Street within the South Farms
area”, providing important connections to the Florida Avenue corridor from the
south. 

One significant non-transportation proposal in this section is the redevelopment
of Orchard Downs apartments, due to their age and deferred maintenance. This
proposal would maintain the complex “as a mixed income, affordable graduate,
married student, and family housing neighborhood” and preserve the current
number of housing units, while also adding 156 units to compensate for the
redevelopment of Ashton Woods graduate housing on the southwest side of campus.
This further emphasizes the need for safe and effective travel options between
Orchard Downs and the rest of campus. Other future developments that could
affect travel along this area include the new construction and redevelopment
ACES facilities along Lincoln Avenue south of Florida Avenue, and the renovation
of athletic facilities along Kirby and Florida, directly west of the corridor.

### Urbana Bicycle Master Plan (UBMP), 2016 

The [2016 Urbana Bicycle Master Plan
(UBMP)](https://ccrpc.org/documents/urbana-bicycle-master-plan-2016/) is an
update of the 2008 plan of the same name. Both plans examine the current status
of bicycle facilities in Urbana, in addition to the conditions that must be met
to allow for higher bicycle usage in the community. The UBMP emphasizes the need
for accessible bicycle facilities that increase cyclist safety. The development
of the UBMP included public workshops in neighborhoods, schools, and municipal
buildings where Urbana residents provided feedback on existing bicycle
facilities and requested new ones. 

#### UBMP Recommendations Related to the Florida Avenue Corridor Study

{{<image src="UBMP PM2 Votes2.jpg"
  alt="Map of public votes from 2016 Urbana Bicycle Master Plan"
  position="right">}}

The UBMP identified the half-mile gap in bike facilities on Florida Avenue from
Race Street to Lincoln Avenue as an improvement priority. Closing this gap would
create a corridor of nearly four miles of continuous bike lanes and sidepaths,
from the eastern terminus of Florida Avenue to the intersection of Kirby Avenue
and Neil Street in Champaign. If Florida Avenue and its sidepath are extended to
High Cross Road/IL 130 in the future, this bikeway corridor will be almost four
and a half miles long, and it will connect to the proposed High Cross Road sidepath
and Kickapoo Rail-Trail. This would be one of the longest bikeways in
Urbana-Champaign. This segment was one of the most requested bikeway projects at
the UBMP's second public workshop as well as in the [2014 Champaign County
Greenways and Trails
Plan](https://ccrpc.org/documents/active-choices-champaign-county-greenways-trails-plan-2014/)
and the [Long Range Transportation Plan (LRTP) 2040: *Sustainable
Choices*](https://ccrpc.org/documents/lrtp-sustainable-choices-2040/).

### Urbana Park District Trails Master Plan, 2016

In conjunction with the UBMP, CCRPC developed the [Urbana Trails Master Plan
(UTMP)](https://ccrpc.org/documents/urbana-park-district-trails-master-plan/)
for the Urbana Park District in 2016. The Urbana Park District manages
approximately 16 miles of trails, including about 11 miles of paved trails
distributed in 18 parks, and about five miles of soft trails found within five
parks. The UTMP will help guide the creation of linkages for existing, proposed,
and future trail facilities that will further enhance the connectivity of area
trails for the enjoyment of area residents and visitors. 

#### UTMP Recommendations Related to the Florida Avenue Corridor Study

A steering committee, multiple public workshops, and 1,371 survey responses
guided the recommendations listed in the UTMP. Among the recommendations are a
series of smaller loops in and around twelve Urbana parks, including extending
and widening the existing sidewalks in Blair Park to facilitate use by people of
all ages and abilities. The Urbana Park District received an Open Space Land and
Development (OSLAD) grant from the Illinois Department of Natural Resources to
implement the [2019 Blair Park Master
Plan](https://www.urbanaparks.org/assets/1/6/2019_Blair_Park_Concept.pdf), which
reflects recommendations from the 2016 UTMP. The improvements are scheduled to
be completed in Fall 2021 and include constructing an eight-foot-wide multi-use
path on all four sides of the park to create a perimeter loop trail.

### Champaign County Greenways and Trails Plan: *Active Choices*, 2014

{{<image src="GTcover-1.jpg"
  alt="Cover of Champaign County Greenways and Trails Plan, 2014"
  position="right">}}

The [Champaign County Greenways and Trails Plan, *Active
Choices*](https://ccrpc.org/documents/active-choices-champaign-county-greenways-trails-plan-2014/),
analyzes existing bike and trail facilities, green spaces, and popular
destinations where visitors may travel by the active modes of bicycling or
walking. CCRPC completed the most recent update of the Greenways and Trails plan
in 2014. This plan reflects the desires of Champaign County residents and
community leaders to improve mobility through safe, efficient, and
well-connected multi-modal transportation infrastructure.  

#### Greenways and Trails Public Input Related to the Florida Avenue Corridor Study

Input collected during public workshops in 2013 were one of several factors used
to develop the plan’s list of prioritized projects. The prioritized list
includes a 0.64 mile shared-use path (off street/sidepath) on the south side of
Florida Avenue between Lincoln Avenue and Race Street. This recommendation is
broken up into two smaller sections. Both sections are of medium priority, but
implementation time frames differ for each section. The 0.41 mile stretch
between Lincoln Avenue and Orchard Street has a medium-priority time frame,
meaning this section should be completed first. The remaining sections receive a
low-priority time frame, meaning they should be constructed after the other
sections have been completed. 
