---
title: "Future Recommendations"
draft: false
weight: 30
---

{{<image src="mode_4_5_all.jpg"
  alt="diagram of future improvements superimposed on an aerial image of the corridor"
  caption= "Right-click to open the image in a new tab" 
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

#### Drive-through rendering of proposed improvements on Florida Avenue, starting at Vine Street and moving west to Lincoln Avenue 

<iframe width="560" height="315" src="https://www.youtube.com/embed/XsqB9gT8Rdg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Drive-through rendering by Lochmueller Group*

## Existing Issues and Related Recommendations

The [existing conditions
documentation](https://ccrpc.gitlab.io/florida-ave/existing-conditions/transportation/),
the [first round of public
input](https://ccrpc.gitlab.io/florida-ave/public-involvement/survey-results/),
and the [scenario evaluation
process](https://ccrpc.gitlab.io/florida-ave/future-scenarios/scenario-evaluation/)
lead the corridor study Working Group and CUUATS staff to identify which
transportation improvements would most effectively meet the existing issues
identified in the corridor and which were most popular among members of the
public. The sections below outline the existing issues and the corresponding
recommendations for each mode of transportation. Several issues and
recommendations are relevant to more than one mode and are therefore repeated
across different sections. 

## Pedestrians

{{<image src="mode_4_5_ped.jpg"
  alt="diagram of future improvements superimposed on an aerial image of the corridor"
  caption= "Right-click to open the image in a new tab" 
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

### Pedestrian-Related Issues

- The [pedestrian level of traffic stress](https://ccrpc.gitlab.io/florida-ave/existing-conditions/transportation/#pedestrian-level-of-traffic-stress) fluctuates between medium and high stress along the corridor.
- There are no sidewalks on the south side of Florida Avenue between Lincoln Avenue and Race Street, a problem reiterated in the public input.
- Public input included complaints about the condition of sidewalks throughout the corridor.
- Public input documented appreciation for safe signalized crossings for pedestrians where they are present. 
- Public input documented confusion for all travel modes resulting from the busy all-way stop-controlled intersections with additional turning lanes at Race Street and Vine Street.

### Pedestrian-Related Recommendations for the Future

*The graphic at the beginning of this section illustrates many of the pedestrian-related recommendations for the future.*

- Add an off-street shared-use path on the south side of Florida Avenue between Lincoln Avenue and Race Street to improve safety, accessibility, and connectivity for pedestrians. 
- Replace damaged sidewalk panels throughout the corridor to improve pedestrian safety and accessibility. 
- Reconstruct the Florida Avenue roadway from Lincoln Avenue to Vine Street to improve safety and accessibility for existing vehicle lanes and on-street bike lanes (including crosswalk repainting and sidewalk ramp reconstruction). If funding is available, it is recommended to continue reconstruction west of Lincoln Avenue to the Urbana city limit at Wright Street. 
- Add new traffic signals at the Florida Avenue intersections with Race Street and Vine Street to improve safety and traffic flow. New traffic signals should also include accessible (audible) pedestrian signals and push button devices, allow for vehicle and bicycle detection, and provide an opportunity to implement Traffic Signal Priority and Preemption to give priority to emergency vehicles and public transit buses when necessary.
- Upgrade traffic signals at the Florida Avenue intersections with Lincoln Avenue and Orchard Street to improve safety and traffic flow. New traffic signals should also include accessible (audible) pedestrian signals and push button devices, allow for vehicle and bicycle detection, and provide an opportunity to implement Traffic Signal Priority and Preemption to give priority to emergency vehicles and public transit buses when necessary.
- Upgrade lighting throughout the corridor to improve safety and accessibility for all users. 

## Cyclists

{{<image src="mode_4_5_bike.jpg"
  alt="diagram of future improvements superimposed on an aerial image of the corridor"
  caption= "Right-click to open the image in a new tab" 
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

### Cyclist-Related Issues

- There are no bicycle facilities between Lincoln Avenue and Race Street, and sidewalks are present only on the north side of the street.
- The [level of traffic stress for cyclists](https://ccrpc.gitlab.io/florida-ave/existing-conditions/transportation/#bicycle-level-of-traffic-stress) is high between Lincoln Avenue and Race Street and medium between Race Street and Vine Street. The high traffic stress for cyclists was backed up by public input which included several respondents saying they avoid biking on Florida Avenue because it doesn't seem safe.
- Public input reported that poor pavement conditions contribute to hazardous cycling conditions.
- Public input documented confusion for all travel modes resulting from the busy all-way stop-controlled intersections with additional turning lanes at Race Street and Vine Street.

### Cyclist-Related Recommendations for the Future

*The graphic at the beginning of this section illustrates many of the cyclist-related recommendations for the future.*

- Add an off-street shared-use path on the south side of Florida Avenue between Lincoln Avenue and Race Street to improve safety, accessibility, and connectivity for cyclists. 
- Add [two-stage turn queue boxes](https://mtd.org/inside/mtd-pulse/how-to-use-a-bike-box-stop-turn-wait/) at the Florida Avenue intersection with Race Street to help cyclists transition between on-street bike lanes and off-street shared-use use paths. 
- Reconstruct the Florida Avenue roadway from Lincoln Avenue to Vine Street to improve safety and accessibility for existing vehicle lanes and on-street bike lanes (including crosswalk repainting and sidewalk ramp reconstruction). If funding is available, it is recommended to continue reconstruction west of Lincoln Avenue to the Urbana city limit at Wright Street.
- Add new traffic signals at the Florida Avenue intersections with Race Street and Vine Street to improve safety and traffic flow. New traffic signals should also include accessible (audible) pedestrian signals and push button devices, allow for vehicle and bicycle detection, and provide an opportunity to implement Traffic Signal Priority and Preemption to give priority to emergency vehicles and public transit buses when necessary.
- Upgrade traffic signals at the Florida Avenue intersections with Lincoln Avenue and Orchard Street to improve safety and traffic flow. New traffic signals should also include accessible (audible) pedestrian signals and push button devices, allow for vehicle and bicycle detection, and provide an opportunity to implement Traffic Signal Priority and Preemption to give priority to emergency vehicles and public transit buses when necessary.
- Upgrade lighting throughout the corridor to improve safety and accessibility for all users.

## Transit Passengers

{{<image src="mode_4_5_bus.jpg"
  alt="diagram of future improvements superimposed on an aerial image of the corridor"
  caption= "Right-click to open the image in a new tab" 
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

### Transit-Related Issues  

- Some of the 14 existing bus stops within the corridor are not frequently utilized.  
- Public input documented appreciation for the bus service from riders as well as a desire for more paved bus stops with shelters. 
- Public input documented concerns about buses contributing to congestion since the roadway is too narrow for private vehicles to pass while buses are stopped.

### Transit-Related Recommendations for the Future

*The graphic at the beginning of this section illustrates many of the transit-related recommendations for the future.*

- Add bus cut-outs to prevent traffic obstruction at the Florida Avenue intersections with Orchard Street (on the SE Corner) and Vine Street (on the NW corner, at Blair Park). Cut-outs will include concrete landing pads and shelters for transit passengers. 
- Consolidate bus stops between Race Street and Vine Street to improve route efficiency (including new stop with bus cut-out on the NW corner of the Florida Avenue and Vine Street intersection at Blair Park). 
- Add an off-street shared-use path on the south side of Florida Avenue between Lincoln Avenue and Race Street to improve safety, accessibility, and connectivity for transit passengers accessing bus stops.
- Reconstruct the Florida Avenue roadway from Lincoln Avenue to Vine Street to improve safety and accessibility for existing vehicle lanes and on-street bike lanes (including crosswalk repainting and sidewalk ramp reconstruction). If funding is available, it is recommended to continue reconstruction west of Lincoln Avenue to the Urbana city limit at Wright Street.
- Add new traffic signals at the Florida Avenue intersections with Race Street and Vine Street to improve safety and traffic flow. New traffic signals should also include accessible (audible) pedestrian signals and push button devices, allow for vehicle and bicycle detection, and provide an opportunity to implement Traffic Signal Priority and Preemption to give priority to emergency vehicles and public transit buses when necessary.
- Upgrade traffic signals at the Florida Avenue intersections with Lincoln Avenue and Orchard Street to improve safety and traffic flow. New traffic signals should also include accessible (audible) pedestrian signals and push button devices, allow for vehicle and bicycle detection, and provide an opportunity to implement Traffic Signal Priority and Preemption to give priority to emergency vehicles and public transit buses when necessary.
- Upgrade lighting throughout the corridor to improve safety and accessibility for all users. 

## Drivers

{{<image src="mode_4_5_car.jpg"
  alt="diagram of future improvements superimposed on an aerial image of the corridor"
  caption= "Right-click to open the image in a new tab" 
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

### Driving-Related Issues

- The intersection of Race Street and Florida Avenue was identified as one of the top 5 percent of priority safety locations in IDOT District 5 in 2017. 
- The intersection of Race Street and Florida Avenue is currently operating at a level of service **D** in the AM peak period and level of service **F** (worst) in the PM peak period.
- In 2045, all four major intersections are projected to be operating at a level of service **D**, **E**, or **F** (worst) in the AM and/or PM peak periods if no transportation improvements are made.
- Between 2014 and 2018, there were 25 crashes at the Race Street and Florida Avenue intersection including one severe-injury crash and zero fatal crashes.
- Public input documented confusion for all travel modes resulting from the busy all-way stop-controlled intersections with additional turning lanes at Race Street and Vine Street.
- Road surface conditions on the majority of Florida Avenue are rated “very poor” according to the City of Urbana's Pavement Condition Index and also according to public input.
- Public input reported a desire to bring back street parking to north side of Florida Avenue between Busey Avenue and Race Street to serve the adjacent households, serve as a traffic-calming measure, and to alleviate traffic obstruction from parked service and delivery vehicles.

### Driving-Related Recommendations for the Future 

*The graphic at the beginning of this section illustrates many of the driving-related recommendations for the future.*

- Reconstruct the Florida Avenue roadway from Lincoln Avenue to Vine Street to improve safety and accessibility for existing vehicle lanes and on-street bike lanes (including crosswalk repainting and sidewalk ramp reconstruction). If funding is available, it is recommended to continue reconstruction west of Lincoln Avenue to the Urbana city limit at Wright Street.
- Add new traffic signals at the Florida Avenue intersections with Race Street and Vine Street to improve safety and traffic flow. New traffic signals should also include accessible (audible) pedestrian signals and push button devices, allow for vehicle and bicycle detection, and provide an opportunity to implement Traffic Signal Priority and Preemption to give priority to emergency vehicles and public transit buses when necessary.
- Upgrade traffic signals at the Florida Avenue intersections with Lincoln Avenue and Orchard Street to improve safety and traffic flow. New traffic signals should also include accessible (audible) pedestrian signals and push button devices, allow for vehicle and bicycle detection, and provide an opportunity to implement Traffic Signal Priority and Preemption to give priority to emergency vehicles and public transit buses when necessary.
- Add a parking lane on the north side of Florida Avenue between Busey Avenue and Race Street (within the existing roadway width).
- Upgrade lighting throughout the corridor to improve safety and accessibility for all users. 

### Other Considered Changes

In addition to the considered changes in intersection controls, several other
changes to the corridor were considered or brought to the Working Group’s
attention but will not be included in the current set of recommendations. Below
are short summaries of two of these issues, and the reasons that they are not part of
the current recommendations for the corridor.

#### Shifting Lincoln Avenue merge to west side of the intersection

{{<image src="lincoln_merge.PNG"
  alt="Image of merge lane on Florida Avenue just east of Lincoln Avenue"
  caption= "Florida Avenue and Lincoln Avenue Intersection, 2021" 
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="right">}}
  
Public feedback included complaints and safety concerns about the narrowing of
Florida Avenue when traveling eastbound at the Lincoln Avenue intersection.
Though the existing merge lane is technically compliant with transportation
standards, respondents noted that the merging of through traffic from two lanes
to one immediately after the intersection was a source of confusion and crashes.
Current traffic volumes and projected future traffic volumes do not warrant
adding vehicle lanes on Florida Avenue east of Lincoln Avenue or reducing
vehicle lanes on Florida Avenue west of Lincoln Avenue, which means traffic will
continue to need to merge in the vicinity of the intersection of Florida Avenue
and Lincoln Avenue. Staff investigated the possibility of extending the merge
lane in its current location, however existing access driveways limit that
option. Staff also investigated the possibility of changing the eastbound
right/through lane into an exclusive right-turn lane and merging eastbound
through traffic into one lane before the intersection. However, this option was
ultimately rejected for a couple of reasons. The primary reason is that forcing
all through traffic into one lane before the intersection could cause long
queues when traffic is stopped at Lincoln Avenue during peak hours, possibly
blocking traffic at other access points west of Lincoln Avenue. In addition,
eastbound right-turn traffic is not high enough to warrant a right-turn-only
lane. Finally, while the post-intersection merge may be a cause of stress and
near-misses, crash reports do not indicate frequent collisions at this location
(just two no-injury crashes were recorded between 2014 and 2018). While
relocating the merge is not being recommended at this time, city staff is
considering options to improve signage and/or road markings in this area, to
improve driver awareness.

#### Reduce pavement width between Busey Avenue and Race Street

{{<image src="buffer_image.PNG"
  alt="Image of striped buffers painted on Florida Avenue between Busey Avenue and Race Street"
  caption= "Striped buffers on Florida Avenue between Busey Avenue and Race Street, 2021" 
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="right">}}

Between Busey Avenue and Race Street, Florida Avenue is currently comprised of a
traffic lane in each direction, with narrow, painted buffers along each curb.
These buffer areas taper away at intersections, in order to make room for
additional turn lanes. Because these buffers are less than seven feet at the
widest, they cannot be used for vehicle storage or travel. They are also not
painted as bike lanes and are therefore not safe for cyclists. To make more
efficient use of the pavement, the Working Group considered narrowing the
pavement along this section, providing two traffic lanes without the extra
pavement space. This would reduce the road maintenance burden for the city and
create more permeable green space along the corridor. However, instead, the
study is recommending that the current roadway width be maintained, and a
parking lane be created along the north side of the road. Prior to the last
decade, the roadway section here featured a vehicle parking lane along the north
side of the road, with the two traffic lanes shifted to the south. This lane was
removed due to the burdens of maintenance, with property owners along the
corridor understanding that it might be returned at some point. Because of the
long block length along this section, returning this parking lane will provide
additional parking for adjacent residents as well as for delivery trucks or
other commercial vehicles that currently block the flow of traffic along this
stretch.

{{<image src="aerial.jpg"
  alt="Aerial images of roadway with different roadway painting in 2020 and 2014"
  caption= "Aerial images of Florida Avenue between Orchard Street and Carle Avenue from 2020 (left) and 2014 (right)" 
  attr="CCGIS"
  position="full">}}

#### Westbound Race Street and Vine Street turn lane consistency (subject to further study)

When traveling westbound on Florida Avenue, through traffic is directed to the
left lane at the Vine Street intersection, while through traffic is directed to
the right lane at the Race Street intersection. This inconsistency can lead to
some driver confusion, as documented in the public input. Although specific
intersection design is beyond the scope of this study, the City of Urbana is
currently overseeing a separate intersection design study to consider this
discrepancy and other intersection needs.

{{<image src="turn_lane.jpg"
  alt="Examples of turn lanes on Florida Avenue"
  caption= "Florida Avenue at Race Street (left) and Vine Street Intersection (right), 2021" 
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

