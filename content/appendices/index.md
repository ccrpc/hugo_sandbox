---
title: "Appendices"
draft: false
menu: main
weight: 60
---

## Turning Movement Counts at Selected Intersections, 2017 and 2018
<rpc-table
url="turning_movement_count.csv"
table-title=" "
source=" Florida Avenue and Lincoln Avenue Counts - City of Urbana, 2018 and all other counts - City of Urbana, 2017">
</rpc-table>

## Intersections LOS for Morning and Afternoon Peak Hours, 2017 and 2018
<rpc-table
url="los_table.csv"
table-title=" "
source=" Florida Avenue and Lincoln Avenue Counts - City of Urbana, 2018 and all other counts - City of Urbana, 2017">
</rpc-table>

## Roadway Segments LOS for Morning and Afternoon Peak Hours, 2016
<rpc-table
url="los_segment_table.csv"
table-title=" "
source=" IDOT, 2016">
</rpc-table>

## Intersections LOS for Morning and Afternoon Peak Hours, 2045 Baseline Scenario 
<rpc-table
url="los_intersection_2045.csv"
table-title=" "
source="">
</rpc-table>

## Segments LOS for Morning and Afternoon Peak Hours, 2045 Baseline Scenario
<rpc-table
url="los_segment_2045.csv"
table-title=" "
source="">
</rpc-table>

## Actuated Coordinated Signalized Intersections LOS for Morning and Afternoon Peak Hours, 2045 Scenario 1 
<rpc-table
url="2045_Scenario1_Coordinated.csv"
table-title=" "
source="">
</rpc-table>

## Actuated Uncoordinated Signalized Intersections LOS for Morning and Afternoon Peak Hours, 2045 Scenario 1 
<rpc-table
url="2045_Scenario1_Uncoordinated.csv"
table-title=" "
source="">
</rpc-table>

## Intersections LOS for Morning and Afternoon Peak Hours, 2045 Scenario 2 
<rpc-table
url="2045_Scenario2.csv"
table-title=" "
source="">
</rpc-table>

## Intersections LOS for Morning and Afternoon Peak Hours, 2045 Scenario 3 
<rpc-table
url="2045_Scenario3.csv"
table-title=" "
source="">
</rpc-table>

## Intersections LOS for Morning and Afternoon Peak Hours, 2045 Scenario 4 
<rpc-table
url="2045_Scenario4.csv"
table-title=" "
source="">
</rpc-table>