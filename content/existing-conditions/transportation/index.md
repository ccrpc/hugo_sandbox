---
title: "Transportation"
draft: false
weight: 5
---

Florida Avenue is one of the most important east-west corridors for the City of
Urbana and the University of Illinois Campus District.  The corridor connects 
offices, retailers, healthcare providers, University facilities, residential
areas, and more, with average daily traffic (ADT) as high as 10,300 vehicles.
This corridor is utilized by all major modes of transportation, including
pedestrians, bicyclists, transit, passenger vehicles, and freight vehicles. To
plan for future transportation improvements, it is important to look at the
existing transportation conditions and understand current traffic
operations, infrastructure conditions, facility geometries, and safety issues.
This section covers the following topics: existing geometric features of
roadways and intersections, traffic flow data, traffic growth characteristics,
intersection and roadway segment operational conditions, crash analysis, access
management, transit service, and pedestrian and bicycle facilities.

- Road surface conditions on the majority of Florida Avenue are rated "very poor."
- The corridor’s evolution from a local street to a minor arterial over the course of several decades has resulted in a roadway with significantly more access points than recommended for its current function and average daily traffic. 
- Traffic volume increased along Florida Avenue between Lincoln Avenue and Orchard Street by 7 percent but
  decreased for the rest of the segments in the corridor between 2011 and 2016.
- The Race Street and Florida Avenue intersection is operating at a level of service "D" in the AM peak
  period and level of service "F" (worst) in the PM peak period.
- Between 2014 and 2018, there were no fatal crashes in this corridor and one
  severe-injury crash at the intersection of Race Street and Florida Avenue.
- Transit ridership declined slightly between 2017 and 2019 in the Florida Avenue corridor.
- There are no bicycle facilities between Lincoln Avenue and Race Street, and sidewalks are present only on the north side of the street. *Bicycle and pedestrian improvements recommended for the corridor are documented in the City of Urbana’s [Bicycle Master Plan](https://www.urbanaillinois.us/bicycle-master-plan) and
[Pedestrian Master Plan](https://ccrpc.org/documents/2020-urbana-pedestrian-master-plan-final-report/).* 
- The level of traffic stress for cyclists is high between Lincoln Avenue and Race
  Street and medium between Race Street and Vine Street.
- The pedestrian level of traffic stress fluctuates between medium and high stress along
  the corridor.

## Geometric Features of Roadways and Intersections

Existing geometric features of the Florida Avenue corridor include roadway functional classification, lane
configurations, roadway widths, presence of shoulders, roadway surface
conditions, intersection geometry, and intersection control types. This data is
collected and maintained by the City of Urbana, University of Illinois, and
CUUATS staff. 

### Functional Classification

Functional classification is a process of categorizing roads primarily based on
mobility and accessibility. Mobility relates to operating speed, level of
service, and riding comfort. It is a measure of the expedited traffic conditions
between origin and destination. Accessibility identifies travel conditions with
increased exit and entry locations. IDOT uses the following classifications for
roadways throughout the state: 

{{<image src="Mobilitiy and access.jpg"
  link="/destination"
  alt="Figure describes relationship between mobility and access"
  attr="Mobility vs. Access" attrlink="https://ops.fhwa.dot.gov/access_mgmt/presentations/am_principles_intro/index.htm"
  position="right">}}

- Principal arterials have the highest level of mobility and the lowest level of
  accessibility. Interstates and other major arterials serve trips through urban
  areas and long-distance trips between urban areas.
- Minor arterials serve shorter trips between traffic generators within urban
  areas; they have more access but a lower level of mobility than interstates
  and other principal arterials.
- Major and Minor Collectors provide both mobility and access by gathering trips
  from localized areas and feeding them onto the arterial network.
- Local streets are lower-volume roadways that provide direct land access but
  are not designed to serve through-traffic.

Florida Avenue is a minor arterial throughout the study area, as shown in the following map.
<iframe src="https://maps.ccrpc.org/facs-ec-trans-functionalclass/"
  width="100%" height="400" allowfullscreen="true">
  Map showing the roadway functional class in Florida Avenue Corridor Study Area of Influence.
</iframe>

### Lane Configuration Along Florida Avenue

This section of Florida Avenue includes two different lane configurations. From
Lincoln Avenue to Race Street, the roadway includes one travel lane in each
direction without any bike lanes. From Race Street to Vine Street, the roadway
includes one travel lane and a bike lane in each direction. 

### Roadway Surface Conditions
 
Poor roadway surface conditions are just one reason the City of Urbana has
engaged in this corridor study and set aside funds for future roadway
improvements.

{{<image src="Figure1.jpg"
  alt="Fatigue Cracking on Florida Avenue pavement between Lincoln Avenue and Orchard Street"
  caption="Figure 1: Fatigue Cracking on Florida Avenue between Lincoln Avenue and Orchard Street, 2020"
  position="right">}}

{{<image src="Figure2.jpg"
  alt="Transverse Cracking on Florida Avenue pavement between Orchard Street and Race Street"
  caption="Figure 2: Transverse Cracking on Florida Avenue between Orchard Street and Race Street, 2020"
  position="right">}}

{{<image src="Figure3_1.jpg"
  alt="Roadway Surface Distress on Florida Avenue pavement between Race Street and Vine Street"
  caption="Figure 3: Roadway Surface Distress on Florida Avenue between Race Street and Vine Street, 2020"
  position="right">}}

Cracking, rutting, and patching are some of the issues affecting roadway surface
conditions along the corridor. The City of Urbana’s pavement management system
includes regular inspections and a Pavement Condition Index (PCI) to monitor
roadway surface conditions in the city. The PCI is a numerical index between 0
to 100 that indicates the general condition of a pavement section. According to
the city, the segments in this corridor averaged a PCI of 31 out of 100 in 2019,
which is considered very poor.

From Lincoln Avenue to Orchard Street, the road surface includes some fatigue
cracking (Figure 1), block cracking, and transverse cracking. Between Orchard
Street and Race Street some transverse cracking (Figure 2), fatigue cracking,
and patching is visible. The segment between Race Street and Vine Street
includes frequent fatigue cracking, block cracking, transverse cracking,
patching, and rutting (Figure 3).

### Intersection Control Types

With high traffic volumes, traffic signal timing becomes an important element in
limiting congestion and keeping traffic flowing. Traffic along the Florida
Avenue corridor flows well during the AM and PM peak hours, due to the traffic
signal coordination efforts by the City of Urbana. Although there are ten
intersections on Florida Avenue within the study area, this analysis
focuses on the four intersections that include intersection control for all directions; Florida
Avenue at Lincoln Avenue, Orchard Street, Race Street, and Vine Street. The
Lincoln Avenue and Orchard Street intersections are signalized, while the Race
Street and Vine Street intersections have all way stop signs. The other six
intersections include stop signs on the approaches to Florida Avenue. The
following map shows the control types for each intersection along Florida
Avenue. 
<iframe src="https://maps.ccrpc.org/facs-ec-trans-intersection-control-types/"
  width="100%" height="400" allowfullscreen="true">
  Map showing the Intersection Control Types in Florida Avenue Corridor Study Area of Influence.
</iframe>

## Access Management

Access management is a systematic means of balancing the access and mobility
requirements of streets and roads. [It involves the coordination between land
use and transportation development practices to manage the location, design, and
operation of driveways, median openings, and street connections to
roadways.](http://www.teachamerica.com/accessmanagement.info/pdf/348NCHRP.pdf)
Implementing access management guidelines helps provide a safe and efficient
transportation system. Access management along the Florida Avenue corridor was
evaluated by four criteria outlined in the [CUUATS Access Management Guidelines
(2013)](https://ccrpc.org/documents/access-management-guidelines/): intersection
functional area, signalized access spacing, unsignalized access spacing, and
driveway spacing. Several locations along the corridor fall short of the
recommendations associated with one or more of the four criteria. The evolution
of this corridor from a local street to a minor arterial over the course of
several decades has resulted in a roadway that includes far more access points
than recommended for safe, efficient travel by modern access management
standards.

### Intersection Functional Area

Intersections are comprised of physical and functional areas. The physical
extent of an intersection is the fixed space within the corners of the
intersection. The functional area extends upstream and downstream of the
intersection and includes the roadway length required for vehicle storage and
maneuvering. The upstream functional area of an intersection depends on the
vehicle queuing (storage length), driver perception-reaction time, and the
distance required for decelerating or stopping. The downstream functional area
of an intersection is the distance required by the driver to clear the
intersection and be able to perceive and react to a conflict downstream of the
intersection. 

Access connections, such as driveways or median openings, are not recommended in
the intersection functional area because they could create conflict points,
which the driver approaching or exiting an intersection may not be able to
negotiate safely. The table below lists the desirable and limiting (necessary) values for
the upstream and downstream functional distances.

<rpc-table
url="distance.csv"
table-title="Desirable and Limiting Functional Distances on Florida Avenue, 2020 "
source=" CUUATS Access Management Guideline, 2013"
source-url="https://ccrpc.org/wp-content/uploads/2015/03/access-management-2013-04-17-final.pdf">
</rpc-table>

The graphic below illustrates that multiple access openings in the corridor fall
within the functional area of the intersections. For example, three driveways
and an unsignalized intersection fall within the 385 feet of limiting functional
distance on the upstream westbound leg of the intersection of Lincoln Avenue and
Florida Avenue. 

#### Illustrated Desirable and Limiting Functional Distances

{{<image src="Access Map Graphic2.jpg"
  alt="Road map highlighting all intersections and driveways within the limiting functional distances in the Florida Avenue study area"
  position="full">}}

### Signalized Access Spacing

Signal spacing is defined as the distance between two successive at-grade
signalized intersections, measured between the closer curb/edge of the
intersection. Optimum spacing of the intersections has a significant impact on
safety, traffic operations, and level of service in the corridor. Each
additional traffic signal reduces travel speed while increasing travel
time/delay. The optimal spacing of signals depends on the cycle length and the
posted speed limits.

According to CUUATS Access Management Guidelines, the recommended minimum
distance between two signalized intersections on a minor arterial such as
Florida Avenue is 1,320 feet. The spacing between the signal at Lincoln Avenue
and the signal at Orchard Street is about 1,260 feet, or 60 feet less than the
recommended distance, which should be considered when evaluating future changes
along the corridor.

### Unsignalized Access Spacing

Unsignalized intersections are more common than signalized intersections and
include stop-controlled, yield controlled, and uncontrolled intersections.
According to CUUATS Access Management Guidelines, the spacing between two
consecutive unsignalized accesses along a minor arterial should be at least 660
feet. The space between unsignalized intersections is less than the minimum
recommendations for most of the unsignalized intersections in the study area.
For example, the 660 feet of Florida Avenue immediately west of the unsignalized
Vine Street and Florida Avenue intersection includes two additional unsignalized
intersections as well as five driveways (see graphic above). 

### Driveway Spacing

Driveway spacing is the distance between two adjacent driveways and is measured
from the nearest edge of one driveway to the nearest edge of the next driveway.
Recommended driveway spacing distance is longer on collectors and arterials
compared to local roadways. The CUUATS Access Management Guidelines recommend
330 feet of space between driveways for minor arterials without commercial
development and 660 feet of space between driveways for commercial roadways. The
space between driveways is less than the minimum recommendations for most of the
driveways in the study area, as illustrated in the graphic above.

## Traffic Flow Data

### Turning Movement Counts

Peak hour turning movement counts help quantify existing traffic operation
conditions at different intersections along the corridor. Vehicle turning
movement counts were collected by RPC staff at the four main intersections in
Fall 2017 and Fall 2018, during three typical weekday peak hour time periods: AM
(7:00 AM to 9:00 AM), Noon (11:00AM to 1:00 PM), and PM (4:00 PM to 6:00 PM).
The peak hour approach volumes shown in the table below were calculated using
the turning movement counts for each approach at the selected intersections.

#### Turning Movement Counts at Selected Intersections, 2017 and 2018

{{<image src="Turning_Movement_Count.svg"
  alt="Peak Hour Turning Movement Counts at Selected Intersections on Florida Avenue"
  caption= "Source: Florida Avenue and Lincoln Avenue Counts - City of Urbana, 2018 and all other counts - City of Urbana, 2017"
  position="full">}}
See [appendices](https://ccrpc.gitlab.io/florida-ave/appendices/#turning_movement_count) for more information.

### 24-Hour Traffic Volume

The following table documents the average daily traffic counts (ADT) along this
corridor in 2011 and 2016, along with the percentage change over this time period. Between 2011 and 2016,
ADT increased between Lincoln Avenue and Orchard Street by approximately 7
percent but decreased for the rest of the study corridor. The largest decrease in
ADT volumes along the study corridor was between Orchard Street and Race Street.

<rpc-table
url="adt.csv"
table-title="Average Daily Traffic (ADT), 2011 and 2016 "
source=" Illinois Department of Transportation, 2011 and 2016 Average Daily Traffic Counts"
source-url="http://www.gettingaroundillinois.com/gai.htm?mt=aadt">
</rpc-table>

The following figures show hourly (12-hour) traffic flow variations along the
Florida Avenue corridor in three segments: between Lincoln Avenue and Orchard
Street, Orchard Street and Race Street, and Race Street and Vine Street. As shown in
these figures, westbound traffic on Florida Avenue is higher than eastbound
traffic during the morning as people travel westward to work and school on the
University campus, City of Champaign, and elsewhere. Eastbound traffic on Florida
is higher than westbound traffic in the afternoon as people return home to
residences in Urbana and farther east and south.

<rpc-chart
url="traffic-flow-lincoln-orchard.csv"
type="line"
colors="blue, lime, red"
switch="true"
y-label="Vehicles per Hour"
legend-position="top"
legend="true"
grid-lines="true"
tooltip-intersect="true"
source=" Illinois Department of Transportation, 2016"
source-url=""
chart-title="Hourly Traffic Flow Variations on Florida Avenue between Lincoln Avenue and Orchard Street, 2016"></rpc-chart>

<rpc-chart
url="traffic-flow-orchard-race.csv"
type="line"
colors="blue, lime, red"
switch="true"
y-label="Vehicles per Hour"
legend-position="top"
legend="true"
grid-lines="true"
tooltip-intersect="true"
source=" Illinois Department of Transportation, 2016"
source-url=""
chart-title="Hourly Traffic Flow Variations on Florida Avenue between Orchard Street and Race Street, 2016"></rpc-chart>

<rpc-chart
url="traffic-flow-race-vine.csv"
type="line"
colors="blue, lime, red"
switch="true"
y-label="Vehicles per Hour"
legend-position="top"
legend="true"
grid-lines="true"
tooltip-intersect="true"
source=" Illinois Department of Transportation, 2016"
source-url=""
chart-title="Hourly Traffic Flow Variations on Florida Avenue between Race Street and Vine Street, 2016"></rpc-chart>

## Speed Study

Speed data was collected by IDOT along the Florida Avenue corridor in 2016.
The figure below shows the average travel speed along the three segments that make up
the corridor. The average travel speed between Lincoln Avenue and Orchard Street
was higher than the posted speed limit for both directions at the time this data
was collected. Travel speed between Orchard Street and Race Street was within
one mile per hour of the speed limit in both directions, while travel speed
between Race Street and Vine Street was consistently lower than the posted speed
limit.

<rpc-chart
url="speed.csv"
type="line"
switch="true"
y-label="Average Travel Speed (MPH)"
legend-position="top"
legend="true"
grid-lines="true"
tooltip-intersect="true"
source=" Illinois Department of Transportation, 2016"
chart-title="Average Travel Speed on Florida Avenue, 2016"></rpc-chart>

## Traffic Operations

Existing traffic operational conditions were evaluated for the four largest
intersections along the Florida Avenue corridor and the three roadway segments
that make up the corridor. Level of Service (LOS) is a qualitative measure
describing operational conditions, from “A” (best) to “F” (worst), within a
traffic stream or at an intersection. The LOS criteria descriptions listed below
include conditions for both intersections and roadways. 

- __LOS A:__ Describes primarily free-flow operations at average travel speeds.
  Drivers have complete freedom to maneuver within the traffic stream and
  through an intersection with minimal delay.
- __LOS B:__ At this level, the ability to maneuver within the traffic stream is
  slightly restricted and represents stable flow with slight delays. Control
  delays at signalized intersections are not significant.
- __LOS C:__ Represents stable operations but the ability to maneuver and change
  lanes in mid-block locations is more complicated than at LOS B. Intersections
  have stable flow with acceptable delays.
- __LOS D:__ This level borders on a range in which small increases in flow may
  cause substantial increases in delay and decreases in travel speed. This
  situation may occur due to adverse signal progression, inappropriate signal
  timing, high volumes, or a combination of these factors. Intersections are
  approaching unstable flow with tolerable delay (e.g. travelers must
  occasionally wait through more than one signal cycle before proceeding).
- __LOS E:__ Traffic flow faces significant delays at this level. Adverse
  progression, high signal density and high volumes are main contributing
  factors to this situation. Intersections have unstable flow with an
  approaching intolerable delay.
- __LOS F:__ This level is characterized by extremely low speeds and a forced or
  jammed flow. Intersection congestion is expected at critical signalized
  locations, with high delays, high volumes, and extensive queuing.

### Operational Conditions of Selected Intersections

Existing traffic operational conditions were evaluated for the four largest
intersections along the Florida Avenue corridor located at Lincoln Avenue,
Orchard Street, Race Street, and Vine Street. Selected intersection criteria
such as Level of Service (LOS), approach delay, and intersection delay were
analyzed to determine the existing operational conditions during the morning and
afternoon peak hours on a typical weekday. LOS is quantified for signalized and
unsignalized intersections using vehicle control delay, or the total delay
experienced by a vehicle due to intersection control measures such as a traffic
signals or stop signs. The highest hourly volumes for the corresponding
approaches were used for analysis purposes. The intersection LOS was completed
using the Synchro 10 software suite, which is based on the methodologies
outlined in the Highway Capacity Manual (HCM 2010) published by the
Transportation Research Board (TRB).

The following tables show the LOS criteria for signalized and unsignalized intersections.

<rpc-table
url="los.csv"
table-title="LOS Criteria for Signalized and Unsignalized Intersections"
source=" HCM 2010"
source-url="">
</rpc-table>

Control delay is determined by comparing the difference between the travel time
that would have occurred in the absence of the intersection control and the
travel time that results from the presence of the intersection control. Average
control delay per vehicle is estimated for each lane group, then aggregated for
each approach and for each intersection overall. Building on the turning
movement data listed above, LOS, approach delays, and intersection delays for
all the selected intersections during morning and afternoon peak hours were
calculated and are presented in the following figure. Among the aggregated LOS
scores for each intersection, none of the four intersections scores higher than
an LOS C during the peak travel hours. The lowest LOS scores include an LOS F at
the intersection of Race Street and Florida Avenue during the PM peak hour, an
LOS D at the same intersection during the AM peak hour, and an LOS D at the
intersection of Orchard Street and Florida Avenue during the AM peak hour.

#### Intersections LOS for Morning and Afternoon Peak Hours, 2017 and 2018

{{<image src="LOS_Intersection.svg"
  alt="Intersections Level of Service for Morning and Afternoon Peak Hours along Florida Avenue"
  caption= ""
  position="full">}}
  See [appendices](https://ccrpc.gitlab.io/florida-ave/appendices/#los_table) for more information.

### Operational Conditions of Roadway Segments

All the roadway segments within the study area include one vehicle lane
traveling in each direction. The vehicular operational conditions of these
roadway segments during morning and afternoon peak hours were analyzed using two
performance measures carried out according to methodologies described in the HCM
2010 Chapter 17: Urban Street Segments. One measure was the travel speed for
through vehicles, which reflects factors that influence running time along the
link and the delay incurred at the boundary intersection. The second measure was
the volume-to-capacity ratio for the through movement at the downstream boundary
intersection. These performance measures indicate the degree of mobility
provided by the segments. 

The following table lists the LOS thresholds established for vehicles on urban
streets.

<rpc-table
url="urban-street-los.csv"
table-title="Urban Street LOS for Automobile Mode"
source=" HCM 2010"
source-url="">
</rpc-table>

The figures below show the LOS calculations for the three roadway segments that
make up the corridor. Speed data collected by IDOT in 2016 was used for this
analysis. LOS values of A or B for all corridor segments indicate roadway
operating conditions are good with minimal restrictions.

#### Roadway Segments LOS for Morning and Afternoon Peak Hours, 2016

{{<image src="LOS_Segment.svg"
  alt="Roadway Segments LOS for Morning and Afternoon Peak Hours on Florida Avenue"
  attr=""
  position="full">}}
See [appendices](https://ccrpc.gitlab.io/florida-ave/appendices/#los_segment_table) for more information.

## Crash Analysis

To identify existing safety issues, staff collected and analyzed the most recent
crash data available from 2014 to 2018 from IDOT, including total number of
crashes, crash types, crash severity, roadway surface, and lighting conditions
when crashes happened.

### Crash Trends

There were 90 reported crashes along the study corridor between 2014 and 2018.
Most of the crashes (88 percent or 79 crashes) occurred at intersections. The
figure below shows the total number intersection crashes and segment crashes
reported per year along Florida Avenue. The highest number of crashes occurred
in 2017 (24 crashes); the lowest number of crashes occurred in 2014 (14
crashes).

<rpc-chart
url="crash-total.csv"
type="line"
switch="true"
y-label="Number of Crashes)"
legend-position="top"
legend="true"
grid-lines="true"
tooltip-intersect="true"
source=" Illinois Department of Transportation, 2014-2018 Person-level Crash Data"
source-url="https://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
chart-title="Traffic Crash Trend, 2014-2018"></rpc-chart>

The table below focuses on the 79 crashes that occurred at intersections along
the corridor. The intersection of Lincoln Avenue and Florida Avenue had the
highest number of crashes (33 crashes) between 2014 and 2018 while the
intersection of Orchard Street and Florida Avenue had the lowest number (5
crashes) during the same period.

<rpc-table
url="crash-intersection.csv"
table-title="Intersection Crashes, 2014-2018"
source=" Illinois Department of Transportation, 2014-2018 Person-level Crash Data"
source-url="https://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data">
</rpc-table>

The following table documents the location and year of the 11 mid-block crashes
along the corridor between 2014 and 2018. The segment between Orchard Street-
Race Street had the highest number of mid-block crashes (5 crashes) while the
segment between Lincoln Avenue and Orchard Street had the lowest number (2
crashes). 

<rpc-table
url="crash-midblock.csv"
table-title="Mid-Block Crashes, 2014-2018"
source=" Illinois Department of Transportation, 2014-2018 Person-level Crash Data"
source-url="https://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data">
</rpc-table>

The following map shows the number of intersection crashes and mid-block crashes
between 2014 and 2018.

<iframe src="https://maps.ccrpc.org/facs-ec-trans-crash-intersection-midblock/"
  width="100%" height="400" allowfullscreen="true">
  Map showing the intersection and mid-block crashes in Florida Avenue Corridor Study Area between 2014 and 2018.
</iframe>

### Crash Types

The figure below shows the predominant crash types at each intersection along
Florida Avenue from 2014 to 2018. As indicated above, angle and rear-end crashes
are the most prevalent crash types at both signalized and unsignalized
intersections, accounting for 46 percent and 28 percent of total intersection
crashes, respectively. Angle crashes, alone, account for 76 percent of total
crashes at the intersection of Race Street and Florida Avenue. 

<rpc-chart
url="crash-intersection-type.csv"
type="horizontalBar"
stacked="true"
x-min="0"
x-max="35"
x-label="Number of Crashes"
legend-position="top"
legend="true"
grid-lines="true"
chart-title="Intersection Crash Types, 2014-2018"
source=" Illinois Department of Transportation, 2014-2018 Person-level Crash Data"
source-url="https://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data">
</rpc-chart>

The table below shows crash types for the mid-block crashes along the corridor,
more than half of which involved parked motor vehicles or rear-end crashes.

<rpc-table
url="crash-midblock-type.csv"
table-title="Mid-Block Crash Types, 2014-2018"
source=" Illinois Department of Transportation, 2014-2018 Person-level Crash Data"
source-url="https://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data">
</rpc-table>

### Crash Severity

The severity of a crash is determined by the most severe injury of a
person in that crash. The crashes are classified in KABCO scale: ‘K’ represents
a fatal crash; ‘A’ represents a crash that caused an incapacitating injury, also
referred as a serious injury; ‘B’ represents a crash that caused a
non-incapacitating injury; ‘C’ represents a crash that caused a reported/not
evident injury; and ‘O‘ represents a crash with no indication of injury and that
just resulted in property damage (PDO).

The table below shows the severity levels of intersection crashes along the
study corridor. There were no fatal intersection crashes in the corridor between
2014 and 2018. In 2017 IDOT identified the intersection of Race Street and
Florida Avenue as one of the top 5 percent priority safety locations in IDOT
District 5. This intersection saw the most injury crashes, including the only
A-injury crash in the corridor, four B-injury crashes, and three C-injury
crashes during the period of analysis. The A-injury crash occurred in May 2015
as a result of disregarding a stop sign. Disregarding a stop sign was the same
cause listed for 10 out of the 17 total (injury and non-injury) crashes at this
intersection during the period of analysis. The intersection of Lincoln Avenue
and Florida Avenue had six injury crashes during the same period as well as 27
PDO crashes. The intersection of Orchard Street and Florida Avenue recorded no
injury crashes and five PDO crashes. The intersection of Vine Street and Florida
Avenue had five injury crashes as well as 11 PDO crashes. The vast majority of
crashes involved passenger vehicles, and no crashes were recorded involving
pedestrians or bicyclists. 

<rpc-table
url="crash-intersection-severity.csv"
table-title="Intersection Crash Severity, 2014-2018"
source=" Illinois Department of Transportation, 2014-2018 Person-level Crash Data"
source-url="https://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data">
</rpc-table>

The map below shows traffic crash severity levels for all the intersection and
mid-block crashes along the corridor between 2014 and 2018.

<iframe src="https://maps.ccrpc.org/facs-ec-trans-crash-severity/"
  width="100%" height="400" allowfullscreen="true">
  Map showing the intersection and mid-block crash severity in Florida Avenue Corridor Study Area between 2014 and 2018.
</iframe>

### Road Surface and Lighting Conditions

The table below shows that many of the intersection crashes (66 percent) along
this corridor occurred during dry road surface conditions. However, 14 of the 33
crashes at the intersection of Lincoln Avenue and Florida Avenue occurred when
the road surface was wet, or snow or slush was present. 

<rpc-chart
url="crash-intersection-pavement.csv"
type="horizontalBar"
colors="#c4c4c4,red,blue,lime"
stacked="true"
x-label="Percent of Crashes"
x-min="0"
x-max="100"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" Illinois Department of Transportation, 2014-2018 Person-level Crash Data"
source-url="https://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
chart-title="Road Surface Conditions on Intersection Crashes, 2014-2018"></rpc-chart>

The following table also shows that most of the intersection crashes (87
percent) occurred during daylight hours.

<rpc-chart
url="crash-intersection-lighting.csv"
type="horizontalBar"
colors="#c4c4c4,red,blue,lime"
stacked="true"
x-label="Percent of Crashes"
x-min="0"
x-max="100"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" Illinois Department of Transportation, 2014-2018 Person-level Crash Data"
source-url="https://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
chart-title="Roadway Lighting Conditions on Intersection Crashes, 2014-2018"></rpc-chart>

## Transit Service

Buses are an excellent and inexpensive option for traveling around the
Champaign-Urbana community, and the rates of people using transit to get to work are
the higher in Urbana and in the area of influence compared with the rest of the
region. Public transit is better for the environment than driving and is
considered a form of “active transportation,” along with bicycling and walking,
due to the additional physical activity required to get to and from bus stops.
Studies show that [transit riders get 8-33 additional minutes of physical
activity each day](https://www.mdpi.com/1660-4601/9/7/2454) compared with
non-transit riders. 

The Champaign Urbana Mass Transit District (MTD) is responsible for providing
fixed-route transit service in the Champaign-Urbana urbanized area. MTD provides
additional transportation services for the University of Illinois as well as
Champaign and Urbana public middle and high school students. The Florida Avenue
corridor currently accommodates four different bus routes, 7-days a week: the
Bronze, Raven, Red, and Teal lines.  

<iframe src="https://maps.ccrpc.org/facs-ec-trans-transit/"
  width="100%" height="400" allowfullscreen="true">
  Map showing the existing bicycle and pedestrian facilities in Florida Avenue Corridor Study Area of Influence.
</iframe>

The following table shows the MTD ridership numbers at the Florida Avenue
corridor bus stops in April and October since 2015. Generally, October
reflects the best picture of the Fall semester service with new students coming
into the town, and ridership is at its highest at this time. Transit ridership fluctuated
between 2015 and 2019, with the highest ridership in 2017. Along the corridor,
Florida Avenue's Orchard Street and Race street bus stops have
the highest ridership numbers. This section of Florida Avenue is the main entry
point for residents of the Orchard Downs housing complex serving approximately
1,200 university-affiliated veterans, students with families, undergraduate
upperclassmen, and graduate students. Households with no vehicle access are also
very significant in this block group and contribute to the higher transit
ridership numbers. 

<rpc-table
url="transit.csv"
table-title="MTD Ridership in April and October 2015, 2017 and 2019"
source=" Champaign-Urbana Mass Transit District"
source-url="">
</rpc-table>

MTD also provides ADA Paratransit service, C-CARTS rural service, Half Fare Cab,
SafeRides, and MTD Connect to make sure people of all ages and abilities can
safely get where they need to go throughout the region. Prior to the COVID-19
pandemic, MTD provided around [twelve million trips
annually.](https://ccrpc.gitlab.io/lrtp2045/existing-conditions/transportation/#transit)
Despite significant ridership reductions during the pandemic, MTD adapted its
existing services and created new services to support riders and drivers through
the health crisis and persisted in providing almost [nine million trips in FY
2020.](https://mtd.org/inside/who-we-are/facts-figures/) 

## Bicycle and Pedestrian Facilities

In 2014, Urbana was named the first Gold-Level Bicycle Friendly Community in
Illinois by the League of American Bicyclists. Urbana rates of biking and
walking to work are the highest in the region, and the corridor’s area of
influence currently includes a variety of on-street and off-street pedestrian
and bicycle facilities including:

- Bike Lane – Road lane dedicated for bicycle travel, alongside vehicle travel. 
- Bike Route – Road with bikeway signage that provides for easy/safe bicycle
  travel with no designated lane for bicycles. 
- Shared-Use Path – Off-street trail, sometimes alongside a road, closed to
  vehicle traffic but open for non-motorized pedestrian and bicycle travel. 
- Sharrow – On-street symbol of a bicycle and two chevrons on top. Indicates
  that bikes and vehicles may use a full lane and is employed when a street is
  too narrow for both a designated bike lane and vehicle lane. 
- Sidewalks – Off-street path along the roadway, primarily for pedestrians. 

On-street bike facilities in the study area include bike lanes on Florida Avenue
east of Race Street with a short section of sharrows at its intersection with
Race Street and Vine Street. The Florida Avenue bike lane connects to bike lanes
on Race Street and bike routes on Broadway Avenue. Orchard Street includes bike
lanes south of Florida Avenue, but they don’t connect to any other bike
facilities in the study area.

Off-street facilities within the influence area consist of sidewalks and
shared-use paths. Along Florida Avenue, there are sidewalks on both sides of the
street, except for the segment between Lincoln Avenue and Race Street where there
is a sidewalk on the north side of the street but not on the south side.
Shared-use paths exist on the south side of Florida Avenue west of Lincoln
Avenue and on the west side of Lincoln Avenue south of Florida Avenue, which are
typically eight to ten feet wide. Both pedestrian and bicycle travel is allowed
on these paths, as there is room for two-way traffic of both bicyclists and
pedestrians. These shared-use paths connect University athletic facilities and
the University Arboretum. Another shared-use path is located on the west side of
Race Street south of Florida Avenue, connecting Florida Avenue to Windsor Road
and Meadowbrook Park.

The segment of Florida Avenue between Lincoln Avenue and Race Street is a
substantial gap in the pedestrian and bicycle network, without any on-street or
off-street bicycle facilities and no sidewalks on the south side of the street.
The 2020 Urbana Pedestrian Master Plan and 2016 Urbana Bicycle Master Plan
identify this gap and recommend a ten-foot-wide concrete shared-use sidepath
along the south side of Florida Avenue between Lincoln Avenue and Race Street.
The recommended path will connect existing east-west and north-south pedestrian
and bicycle facilities on the surrounding streets.

<iframe src="https://maps.ccrpc.org/facs-ec-trans-bike-ped-facilities/"
  width="100%" height="400" allowfullscreen="true">
  Map showing the existing bicycle and pedestrian facilities in Florida Avenue Corridor Study Area of Influence.
</iframe>

## Bicycle and Pedestrian Network Analysis 

Three different tools are used to measure pedestrian and bicycle accommodation
in the corridor area of influence:
- Bicycle Level of Service (BLOS);
- Bicycle Level of Traffic Stress (BLTS); and 
- Pedestrian Level of Traffic Stress (PLTS)

Bicycle Level of Service (BLOS) is an established tool that was used in the 2008
and 2016 City of Urbana Bicycle Master Plans. In addition, CCRPC has recently
developed local analyses for Bicycle Level of Traffic Stress (BLTS) and
Pedestrian Level of Traffic Stress (PLTS) as part of the [CUUATS Sustainable
Neighborhoods Toolkit.](https://ccrpc.org/documents/sustainable-neighborhoods-toolkit/)

### Bicycle Level of Service

Bicycle Level of Service (BLOS) is a nationally recognized tool for determining
the on-road comfort level of bicyclists.[^1]  BLOS quantifies the“bike-friendliness”
of a roadway by using a combination of roadway geometry and traffic conditions
to [calculate a numerical score](https://rideillinois.org/blos/blosform.htm),
between 0 and 5, that is then translated into a grade, A through F. The lower
the numerical score, the better the grade of that street. Roadways with a better
(lower) score are more attractive – and usually safer – for cyclists. 

The following map shows the BLOS grades for the roadway segments that were
evaluated in the corridor’s area of influence. Streets with bike facilities tend
to receive the best BLOS grades. A BLOS grade of A indicates that children and
novice riders would feel comfortable riding on the segment. The portion of
Florida Avenue east of Vine Street is one of the only segments that receives a
BLOS grade of A in the area of influence. The BLOS grades get gradually lower on
Florida Avenue from east to west with a grade of B from Vine Street to Race
Street, C from Race Street to Busey Avenue where no bicycle facilities exist and
the road narrows, and D west of Busey Avenue where there is a shared use path
west of Lincoln Avenue but also higher traffic volumes and the presence of
heavier trucks and freight vehicles. The only other segment to receive a grade
of D in the area is Vine Street south of Florida Avenue, also due to high
traffic volume and/or the presence of heavy trucks and freight vehicles. The
other segments receiving A’s within the area are Pennsylvania Avenue between
Vine Street and Anderson Street, and College Court between Virginia Drive and
Lincoln Avenue. The College Court segment achieved BLOS grade A due to low
vehicle traffic volumes and the presence of one-way thru lanes with sidewalk
facilities. The majority of residential streets in the area exhibit moderate
bike-friendliness with BLOS grades of B or C and no segments in the area
received the lowest BLOS grades of E or F.

<iframe src="https://maps.ccrpc.org/facs-ec-trans-blos/"
  width="100%" height="400" allowfullscreen="true">
  Map showing the existing bicycle level of service within the Florida Avenue Corridor Study Area of Influence.
</iframe>

### Bicycle Level of Traffic Stress

[Developed by the Mineta Transportation Institute](https://transweb.sjsu.edu/research/Low-Stress-Bicycling-and-Network-Connectivity), BLTS rates how stressful a
segment or crossing is for bicyclists on a numeric scale from 1 to 4, with a
score of ‘1’ indicating the lowest level of stress and ‘4’ indicates the highest
level of stress. It evaluates street segments, crossings and intersections, and
turning lanes. BLTS scores consider several factors including street width (thru
lanes per direction), bike lane width, speed limit or prevailing speed, bike
lane blockage, presence of crossing signals, and turning lane configuration.
BLTS scores follow a “weakest link” logic which means that if most of the
segments on a route have a LTS 1 or 2, but one or more segments on the same
route has a LTS 3, the entire route is a LTS 3.

The following map shows BLTS scores in the corridor’s area of influence.
Features like buffered bicycle lanes and low traffic counts decrease BLTS, while
higher posted speed limits and more vehicle travel lanes increase it. BLTS is
higher on Florida Avenue than many of the surrounding roadways, likely because
bicyclists share space with higher traffic volumes traveling at higher speeds.
Bicyclist stress gradually lowers from west to east with a BLTS score of 4 from
Lincoln Avenue to Race Street where no bicycle facilities exist, and a BLTS score
of 3 east of Race street where there are bike lanes and more vehicle traffic with
intersecting streets, increasing the stress level for cyclists. West of Lincoln
Avenue, Florida Avenue has a BLTS score of 1 where there is an off-street shared
use path.

<iframe src="https://maps.ccrpc.org/facs-ec-trans-blts/"
  width="100%" height="400" allowfullscreen="true">
  Map showing the existing bicycle level of traffic stress within the Florida Avenue Corridor Study Area of Influence.
</iframe>

### Pedestrian Level of Traffic Stress

Adapted from the methodology used for the BLTS, PLTS scores consider several
factors including presence of pedestrian facilities, sidewalk width and
condition, general land use types, presence of a median, number of crossing
lanes, and other crossing criteria. The PLTS method uses the same weakest link
logic as BLTS.

PLTS scores fluctuate from 2 to 4 along Florida Avenue. Between Lincoln Avenue
and Busey Avenue, the PLTS score is 2, medium stress, which is the same as
Lincoln Avenue south of Florida Avenue where there are off-street shared-use
path facilities. The PLTS increases to a score of 3 between Busey Avenue and
Orchard Street and Carle Avenue and Race Street where there are narrow sidewalks
on one side of the road. Segments between Orchard Street and Carle Avenue, Race
Street and Broadway Avenue, and east of Vine street received a PLTS score of 4,
high stress for pedestrians. These segments have higher ADT with inadequate
sidewalk facilities increasing pedestrian stress along the corridor. 

The previously mentioned shared-use path proposed on Florida Avenue between
Lincoln Avenue and Race Street in the [2020 Urbana Pedestrian
Plan](https://ccrpc.org/documents/2020-urbana-pedestrian-master-plan-final-report/)
and the [2016 Bicycle Master
Plan](https://ccrpc.org/documents/urbana-bicycle-master-plan-2016/) will fill a
significant gap in the pedestrian and bicycle networks and reduce BLTS and PLTS
scores by reducing vehicle interaction along the corridor. This will also
connect existing east-west and north-south pedestrian and bicycle facilities on
the surrounding streets.

<iframe src="https://maps.ccrpc.org/facs-ec-trans-plts/"
  width="100%" height="400" allowfullscreen="true">
  Map showing the existing pedestrian level of traffic stress within the Florida Avenue Corridor Study Area of Influence.
</iframe>

[^1]: Landis, Bruce. Real-Time Human Perceptions: Toward a Bicycle Level of Service. Transportation Research Record 1578, Transportation Research Board, Washington DC, 1997.