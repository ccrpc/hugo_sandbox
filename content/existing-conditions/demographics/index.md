---
title: "Demographics"
draft: false
weight: 10
---

Learning about the residents of the Florida Avenue corridor—including their attributes 
such as race, ethnicity, income, and disability—allows for a clearer picture of the 
current mobility needs of the area. As described below,
high proportions of the study area residents are college-age students,
households below the poverty line, and residents who own cars but still choose
to walk, bike or use transit at higher than average rates. Transportation
improvements should support active transportation choices and close gaps
in the active transportation network, while accommodating the thousands of
automobile trips that pass through the corridor daily.

The north side of Florida Avenue connects to a predominantly residential 
urban street grid network, with a large public park at the
northwest corner of Vine Street and Florida Avenue. The south side of Florida
Avenue also connects to a residential urban street grid on the east end of the corridor (between Vine Street and Race Street), while the southwest end connects to a more suburban street pattern, including
university housing and open space. The intersection of Orchard Street and
Florida Avenue serves as the main entry point for residents of the Orchard Downs
housing complex, which serves approximately 1,200 university-affiliated veterans,
students with families, undergraduate upperclassmen, and graduate students. The
Orchard Downs complex also includes a community center and a preschool. In addition
to Orchard Downs, student housing—in the form of apartments and shared houses—is 
also found throughout the area of influence, due to its proximity to campus.
The presence of tens of thousands of students attending the University of
Illinois flagship campus contributes to many of the significant differences
between the demographics of this area of influence and the city, and those of the state and
the nation.

{{<image src="corridor_small.jpg"
  alt="Aerial photograph of of corridor study area with labels for existing land uses and transportation infrastructure"
  caption= ""
  position="full">}}

The demographic data in this section come from the U.S. Census Bureau’s American
Community Survey (ACS). The smallest unit of measurement for the ACS is called a
block group. The corridor’s area of influence includes parts of five block groups,
which are used to summarize the general population in the corridor area
(outlined in the map below). Together, the five block groups contain an estimated
population of 7,956 people in 2,632 households, according to the 2015-2019
American Community Survey 5-Year Estimates.

<iframe src="https://maps.ccrpc.org/facs-ec-demo-blockgroups/"
  width="100%" height="400" allowfullscreen="true">
  Map showing the block groups within the Florida Avenue Corridor Study Area of Influence.
</iframe>

## Age Distribution

To identify distinctive characteristics of this area, ACS data from the Florida Avenue area of
influence are compared with city and county-level data. The population graph
below shows that the proportion of college-age adults is larger in the area of
influence than it is at the city or county level. Correspondingly, the
proportion of adults age 65 and older in the area of influence is smaller than it is
at the city and county levels.

<rpc-chart
url="age.csv"
type="horizontalBar"
stacked="true"
x-label="Percent of Population"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B01001"
source-url="https://data.census.gov/cedsci/table?text=B01001&g=0500000US17019_1500000US170190056004,170190057011,170190058004,170190058005,170190060001_1600000US1777005&tid=ACSDT5Y2019.B01001&hidePreview=false"
chart-title="Age Distribution, 2019"></rpc-chart>

## Race and Ethnicity

Within the area of influence, about 65 percent of residents identify as white
alone, 19 percent identify as Asian alone, 10 percent identify as black or
African American alone, and 4 percent identify as two or more races. The
proportion of area residents who identify as Asian alone is similar to the
city’s proportion and 9 percent larger than the county’s, while the proportion
of residents who identify as black or African American alone is lower than the
proportion in both the city and the county. 

<rpc-chart
url="race.csv"
type="horizontalBar"
colors="blue,orange,#E24ACB,green,indigo,lime,red"
stacked="true"
x-label="Percent of Population"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B02001"
source-url="https://data.census.gov/cedsci/table?text=B02001&g=0500000US17019_1500000US170190056004,170190057011,170190058004,170190058005,170190060001_1600000US1777005&tid=ACSDT5Y2019.B02001&hidePreview=false"
chart-title="Race and Ethnicity, 2019"></rpc-chart>

The racial and ethnic makeup of the influence area is more diverse than the
county overall. The percentage of minority racial groups in the influence area
is about 7 percent higher than in the county. Residents of Hispanic or Latino
origin make up about 11 percent of the area of influence population, a higher
percentage than in both the city and the county.

<rpc-chart
url="hispanic.csv"
type="bar"
colors="#c4c4c4, #24ABE2"
x-label="Percent of Population"
x-min="0"
x-max="45"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B03003"
source-url="https://data.census.gov/cedsci/table?text=B03003&g=0500000US17019_1500000US170190056004,170190057011,170190058004,170190058005,170190060001_1600000US1777005&tid=ACSDT5Y2019.B03003&hidePreview=false"
chart-title="Hispanic and Minority Groups, 2019">
<rpc-dataset
type="line"
fill="false"
border-color="grey"
border-width="3"
point-radius="0"
order="1"
label="National Percentage of Hispanic Population in 2019"
data="18, 18, 18">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="blue"
border-width="3"
point-radius="0"
order="1"
label="National Percentage of Minority Groups in 2019"
data="27.5, 27.5, 27.5">
</rpc-chart>


## Households with Disabilities

The ACS estimates 285 households within the area of influence (11 percent)
include one or more persons with a disability. According to the [American
Association of People with Disabilities
(AAPD)](https://www.aapd.com/advocacy/transportation/), residents with
disabilities disproportionately utilize non-automobile transportation options,
such as pedestrian facilities and buses. To assess the [Americans with Disabilities
Act (ADA)](https://www.ada.gov/ada_title_II.htm) compliance of the regional
pedestrian network, CUUATS maintains the [Sidewalk Explorer
Application](https://ccrpc.gitlab.io/sidewalk-explorer/). Sidewalk Explorer
documents the compliance scores for pedestrian facilities, which play a role in
the [Pedestrian Level of Traffic Stress
scores](https://ccrpc.gitlab.io/florida-ave/existing-conditions/transportation/#pedestrian-level-of-traffic-stress)
documented in the
[Transportation](https://ccrpc.gitlab.io/florida-ave/existing-conditions/transportation/)
section. MTD ensures transit accessibility by making sure all fixed-route buses
are ADA accessible, as well as offering ADA Paratransit Service, a curb-to-curb
transportation service available to persons with disabilities who are unable to
use fixed route bus services. 

<rpc-chart
url="disability.csv"
type="bar"
colors="#24ABE2, #c4c4c4"
stacked="true"
x-label="Percent of Households"
x-min="0"
x-max="100"
wrap=17
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table B22010"
source-url="hhttps://data.census.gov/cedsci/table?text=Table%20B22010&g=0100000US_0500000US17019_1500000US170190056004,170190057011,170190058004,170190058005,170190060001_1600000US1777005&tid=ACSDT5Y2019.B22010&hidePreview=false"
chart-title="Percentage of the Households with a Disability, 2019">
<rpc-dataset
type="line"
fill="false"
border-color="red"
border-width="3"
point-radius="0"
order="1"
label="National Percentage of Households with A Disability in 2019"
data="25.5, 25.5, 25.5">
</rpc-dataset>
</rpc-chart>

## Income and Poverty

The income to poverty ratio describes the extent to which a household’s income
is above or below the poverty threshold. An income to poverty ratio of less than
one indicates a household income below the poverty threshold. An income to
poverty ratio of less than 0.50 represents a household income at less than 50
percent of the poverty threshold. According to 2019 ACS data, about 19 percent of residents in the
of the area of influence live in a household with an income below the
poverty threshold, compared to about 30 percent of Urbana residents and 21
percent of county residents. These high local levels of poverty can at least
partially be attributed to the university student population, which is largely
supported by student loans and other outside funding sources while studying at
the University of Illinois.   

<rpc-chart
url="poverty.csv"
type="horizontalBar"
switch="false"
colors="red,#c77f87,gray,#c4c4c4,#DDDDDD"
stacked="true"
legend-position="top"
legend="true"
x-label="Percentage of Population"
x-min="0"
x-max="100"
wrap=17
tooltip-intersect="true"
source=" U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table C17002"
source-url="https://data.census.gov/cedsci/table?text=C17002&g=0500000US17019_1500000US170190056004,170190057011,170190058004,170190058005,170190060001_1600000US1777005&tid=ACSDT5Y2019.C17002&hidePreview=false"
chart-title="Percentage of Population Living below the Poverty Level, 2019"> </rpc-chart>


<rpc-chart
  url="belowpoverty.csv"
  type="bar"
  colors="#24ABE2"
  stacked="false"
  legend-position="top"
  legend="true"
  x-label="Percentage of Population"
  x-min="0"
  x-max="40"
  wrap=17
  tooltip-intersect="true"
  chart-title="Percentage of Population Living Below the Poverty Level, 2019"
  source-url="https://data.census.gov/cedsci/table?text=C17002&g=0100000US_0500000US17019_1500000US170190056004,170190057011,170190058004,170190058005,170190060001_1600000US1777005&tid=ACSDT5Y2019.C17002&hidePreview=false"
  source=" U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table C17002">
  <rpc-dataset
  type="line"
  fill="false"
  border-color="red"
  border-width="4"
  point-radius="0"
  order="1"
  label="National Percentage of Population Living Below the Poverty Level in 2019"
  data="13.4,13.4,13.4">
  </rpc-dataset>
  </rpc-chart>

## Household Access to Vehicles

Household access to motor vehicles is one variable that can help illustrate the
relationship between income and mode choice. According to 2019 ACS data, about
12 percent of influence area households and 19 percent of Urbana households
don’t have access to a vehicle. Zero-vehicle households are generally more
common in densely populated student housing areas and high-poverty neighborhoods
where residents can't afford vehicle ownership. Higher levels locally could also
be attributed to university efforts to minimize student vehicles in the campus
district, the high cost of vehicle ownership, and improvements in walking,
biking, and transit infrastructure that reduce reliance on vehicles. The percent
of zero-vehicle households in the influence area is closer to the lower county
average than the higher city average, despite a higher level of student-age
adults in the influence area and closer proximity to the campus district. However, the
commuting behavior described in the next section shows that a significantly lower
percentage of residents in the influence area drive to work than in both the city
and county. 

<rpc-table
url="vehicles.csv"
table-title="Household Access to Motor Vehicles, 2019"
source=" U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table S0801"
source-url="https://data.census.gov/cedsci/table?text=S0801&g=0500000US17019_1500000US170190056004,170190057011,170190058004,170190058005,170190060001_1600000US1777005&tid=ACSST5Y2019.S0801&hidePreview=false">
</rpc-table>

<rpc-chart
  url="novehicles.csv"
  type="bar"
  colors="#24ABE2"
  stacked="false"
  legend-position="top"
  legend="true"
  x-label="Percentage of Households"
  x-min="0"
  x-max="25"
  wrap=17
  tooltip-intersect="true"
  chart-title="Percentage of Households with No Motor Vehicle Access, 2019"
  source-url="https://data.census.gov/cedsci/table?text=S0801&g=0500000US17019_1500000US170190056004,170190057011,170190058004,170190058005,170190060001_1600000US1777005&tid=ACSST5Y2019.S0801&hidePreview=false"
  source=" U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table S0801">
  <rpc-dataset
  type="line"
  fill="false"
  border-color="red"
  border-width="4"
  point-radius="0"
  order="1"
  label="National Percentage of Households with No Vehicle Access in 2019"
  data="8.6,8.6,8.6">
  </rpc-dataset>
  </rpc-chart>

## Commuting Behavior

According to commuting data for workers 16 years and over, the majority of
influence area residents choose to drive alone to work, rather than walking,
biking, or taking transit. However, this rate of driving alone is still
significantly lower than the regional average. Approximately 44 percent of the
influence area commuters choose to drive alone to work compared with about 55
percent of commuters in Urbana and 71 percent of commuters county-wide.
Alternatively, commuters in the influence area choose to walk, bike and use
transit at much higher rates than in the city and county overall. The ACS
estimates that 20 percent of influence area commuters and 16 percent of
Champaign commuters walked to work in 2019, compared with just eight percent in
the county overall. Public transit was the next most common mode. Similar to the
walking trends, more commuters in the influence area use public transportation
than in the city or county. Almost 15 percent of commuters in the influence area took
the bus to work each day, which is almost three times the county average of five
percent.

<rpc-chart
url="transportation.csv"
type="horizontalBar"
colors="orange,blue, #E24ACB,green,indigo,lime,red"
stacked="true"
switch="false"
x-label="Percent of Workers Over 16 Years"
x-max="100"
wrap=17
legend-position="top"
legend="true"
grid-lines="true"
tooltip-intersect="true"
chart-title="Commuting Mode for Workers 16 Years and Older, 2019"
source=" U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table S0801"
source-url="https://data.census.gov/cedsci/table?text=S0801&g=0500000US17019_1500000US170190056004,170190057011,170190058004,170190058005,170190060001_1600000US1777005&tid=ACSST5Y2019.S0801&hidePreview=false">
</rpc-chart>


Despite higher than average rates of walking, biking, and taking transit to
work, about 80 percent of the influence area workers have a commute of less than 20
minutes, compared with 78 and 64 percent of workers at the city and county levels,
respectively. The significance of shorter commute times combined with higher
rates of walking, biking, and transit use speaks to the short distances residents
are traveling to their workplaces and the prevalence of walking, biking, and
transit infrastructure.

<rpc-table
url="traveltime.csv"
table-title="Commuting Travel Time for Workers 16 Years and Older, 2019"
source=" U.S. Census Bureau, 2015-2019 American Community Survey 5-Year Estimates, Table S0801"
source-url="https://data.census.gov/cedsci/table?text=S0801&g=0500000US17019_1500000US170190056004,170190057011,170190058004,170190058005,170190060001_1600000US1777005&tid=ACSST5Y2019.S0801&hidePreview=false">
</rpc-table>
