---
title: "Environment"
draft: false
weight: 40
---

This section covers some of the environmental features within the corridor that
could be relevant to future transportation improvements. The following tables include the environmental features and regulated substances that were reviewed for their presence in the corridor study’s area of influence. For each of the seven features that are present in the area of influence (topography, soil, wetlands, natural areas, watersheds, drainage districts, and regulated substances) additional information can be found in the paragraphs below.

<rpc-table
url="features_list.csv"
table-title="Environmental Features in the Florida Avenue Corridor Study Area of Influence, 2020">
</rpc-table>

<rpc-table
url="substances_list.csv"
table-title="Regulated Substances in the Florida Avenue Corridor Study Area of Influence, 2020">
</rpc-table>

No significant environmental barriers have emerged in this initial review of the current
conditions; however, a more detailed review of environmental impacts would occur
if specific improvement projects are identified and funded. Regional trends,
priorities, and concerns related to other environmental resources such as air
quality, water quality, noise, and air pollution are also available in the [Long
Range Transportation Plan: 2045](https://ccrpc.gitlab.io/lrtp2045/) and
[Champaign County Regional Environmental
Framework](https://ccrpc.org/documents/ref/).

## Topography 

Consistent with the rest of the county’s flat terrain,  elevation in
the corridor’s area of influence fluctuates between 221 meters (725 feet) and
233 meters (765 feet) above mean sea level. The highest elevations occur on the
southern edges of Orchard Street and west of Goodwin Avenue. The lowest
elevation exists in the northeast section of the area of influence. With a
difference of only 12 meters (40 feet), travelers in the area most likely do not
even notice a change in the elevation.

#### Topography of Florida Avenue Corridor Study Area of Influence
{{<image src="Topography.svg"
  alt="Topography of Florida Avenue Corridor Study Area of Influence"
  caption= ""
  position="full">}}

## Soil

Within the area of influence, there are multiple soil types, each with unique
properties and development potential. The table below describes each soil type.

<iframe src="https://maps.ccrpc.org/facs-ec-env-soil/"
  width="100%" height="400" allowfullscreen="true">
  Map showing soil data within the Florida Avenue Corridor Study Area of Influence.
</iframe>

<rpc-table
url="soil.csv"
table-title="Soil Types in Florida Avenue Corridor Study Area of Influence, 2020"
source=" USDA Web Soil Survey. Accessed December 2020."
source-url="https://www2.illinois.gov/epa/Pages/default.aspx">
</rpc-table>

Flanagan silt loam with 0 to 2 percent slopes makes up almost half of the study
area (47 percent). This is a non-hydric yet poorly drained soil with somewhat
limited development potential. About 22 percent of the study area has hydric
soils; particularly southwest of Lincoln Avenue, west of Orchard Street and east
of Race Street. Although hydric soils present the biggest challenges to
residential or commercial development due to limited water infiltration and
oversaturated pores, these types of soils do not significantly limit
infrastructure development because of their workability (quality moisture
retention), ease of compaction and controlled surface runoff through sloping and
drainage. Native vegetation also can help stabilize hydric soils by providing
natural drainage.

## Wetlands

Wetlands are defined by IDOT as those areas that are “inundated or saturated by
surface or groundwater at a frequency and duration to support, and that under
normal circumstances do support, a prevalence of vegetation typically adapted
for life in saturated soil conditions.” [^1]  The National Wetlands Inventory of the US Fish and Wildlife Service notes a
3.6 acre freshwater emergent wetland habitat northwest of the St. Marys Road and
Lincoln Avenue intersection.
This location’s current use as a university athletic field indicates that there
would likely not be any issues in proceeding with new infrastructure development
plans in the study area, though an IDOT Environmental Survey Request would be
needed to confirm this.

## Natural Areas

Natural areas and open space are defined as areas with natural character, as
evidenced by the presence of vegetation. These include, but are not limited to,
parks and wooded areas.

<iframe src="https://maps.ccrpc.org/facs-ec-env-natural/"
  width="100%" height="400" allowfullscreen="true">
  Map showing natural areas within the Florida Avenue Corridor Study Area of Influence.
</iframe>

- __Blair Park__ is a public park located northwest of the intersection of Florida
  Avenue and Vine Street, adjacent to residential neighborhoods and within a
  quarter-mile walking distance of Carle Park, Sunnycrest Tot Lot, and Crestview
  Park. Bike Routes exist on the north and west side of the park. On the south
  side, bicycle lanes are located on Florida Avenue and shared lane markings or
  sharrows exist where Florida Avenue intersects with Race Street and Vine
  Street. Blair Park is also within walking distance of Urbana Middle School,
  Urbana High School, and Wiley Elementary School. Park visitors accessing Blair
  Park on foot or by bicycle from the east only have an all-way stop to cross
  Vine Street at Florida Avenue. Options for safe crossings should be considered
  to ensure adequate connections for pedestrians and cyclists to this area.

- __Sunnycrest Tot Lot__ is a public park with play equipment
  geared toward small children. This park is bordered by residential
  neighborhoods and is located within a quarter-mile walking distance from Blair
  Park and Crestview Park. There are no sidewalk or bicycle facilities within
  the block to access the park. Pedestrian and bicycle connections exist via
  Florida Avenue, Vine Street, Colorado Avenue and Anderson Street.

- __Grasslands, woodlands, and open space__ provide optimal conditions for many
  native plant and animal species. Approximately 17 acres of wooded area fall
  within the area of influence. The largest site, at 7.7 acres, is north of the
  University of Illinois Arboretum around the University President’s house.
  These sites do not have any regulatory protection other than standard property
  rights, but they are recognized and maintained for their ecological value. In
  addition to the designated wooded areas, 2.7 acres of prairie plantings exist
  on the southwest corner of the Florida Avenue and Orchard Street intersection,
  reflecting campus-wide sustainability efforts. The edge of prairie plantings
  immediately adjacent to Florida Avenue could be impacted by the installation
  of a sidewalk or shared-use path.

## Watersheds

Watersheds are the area of land that drains to one stream, lake, or river. These areas affect the water quality of the body of water they drain into, as well as providing a host of ecosystem services, including nutrient cycling, carbon storage, erosion/sedimentation control, water storage and filtration, flood control, timber and food resources, and recreational opportunities. Watersheds follow their area’s topography from the highest ridgeline of the watershed to the lowest point of land.  The area of influence includes portions of the Vermilion and Embarras watersheds. The Middle Fork and Salt Fork of the Vermilion River are the major waterways that drain the Vermilion watershed, while the Embarras River is the primary drainage waterway for the Embarras watershed.

<iframe src=https://maps.ccrpc.org/facs-ec-env-watershed/
  width="100%" height="400" allowfullscreen="true">
  Map showing the watershed boundaries within the Florida Avenue Corridor Study Area of Influence.
</iframe>


## Drainage Districts

Drainage districts are public corporations with specific governmental functions,
formed to help ensure that there is adequate drainage and flood protection for
the lands lying within the district.[^3] The area of influence includes two
drainage districts, the Urbana-Champaign Sanitary District and Upper Embarras
River Basin Drainage District, which should be consulted in the coordination of
projects in the area.

## Regulated Substances 

Federal and state regulations require that all currently known and potential
special waste sites be identified as part of the environmental review process,
so special preparations can be made to handle contaminants appropriately.
[Special waste refers to any potentially infectious medical waste (PIMW),
hazardous waste, pollution control waste, or industrial process
waste](https://www2.illinois.gov/epa/
topics/waste-management/waste-disposal/special-waste/Pages/default.aspx). The
presence of hazardous or regulated substances affects both human and ecological
health, and work in or around any identified special waste sites can cause a
release of contaminants into the air, soil, and/or water. 

Two leaking underground storage tanks (LUSTs) are located within the influence
area. [LUSTs involve the release of a fuel product from an underground storage
tank that can contaminate surrounding water sources (both surface and ground),
soil, or affect indoor air
quality.](https://www.epa.gov/ust/leaking-underground-storage-tanks-corrective-action-resources#intro)
If a project requires a right-of-way acquisition and there is a site within the
minimum search distance of that property, then a Preliminary Environmental Site
Assessment (PESA) is performed according to Illinois State Geological Survey
Standards.[^2]

<iframe src="https://maps.ccrpc.org/facs-ec-env-lust/"
  width="100%" height="400" allowfullscreen="true">
  Map showing the LUSTs within the Florida Avenue Corridor Study Area of Influence.
</iframe>

<rpc-table
url="regsubstances.csv"
table-title="LUSTs in Florida Avenue Corridor Study Area of Influence, 2020"
source=" NEED SOURCE"
source-url="https://www2.illinois.gov/epa/Pages/default.aspx">
</rpc-table>


[^1]: [IDOT Bureau of Design and Environment (BDE) Manuel](https://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Guides-&-Handbooks/Highways/Design-and-Environment/Design%20and%20Environment%20Manual,%20Bureau%20of.pdf), Chapter 26: Special Environmental Analysis, Accessed 2/17/2021.

[^2]: IDOT SPecial Waste Screening Form D5 P10201

[^3]: [Uchtmann, D.L. & Gehris, B. (1997, December). Illinois Drainage Law.](http://www.farmdoc.illinois.edu/legal/pdfs/drainage_law1.pdf)