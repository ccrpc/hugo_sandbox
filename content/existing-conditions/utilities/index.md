---
title: "Utilities"
draft: false
weight: 50
---

This section provides an overview of existing utilities in and around the study
area. Information was collected about existing sanitary and storm sewer lines,
water mains, fire hydrants, street lighting posts, and conduits. Utility locations
on the maps are shown to the accuracy of information provided by the City
of Urbana Public Works Engineering Division, the Champaign County GIS
Consortium, and surface evidence of their locations.

## Sanitary & Storm Sewer 

The Urbana Department of Public Works and University of Illinois each maintain
sanitary and storm sewer systems within their respective jurisdictions. The
following map shows the distribution of sanitary mains, sanitary pump stations,
sanitary lift stations, and storm pump stations in the influence area. The
sanitary sewer system carries wastewater from homes and businesses to the
Urbana-Champaign Sanitary District’s Northeast Wastewater Treatment Plant. The
storm sewer system carries rainwater runoff from roadways, homes, and businesses
to local creeks and streams. 

<iframe src="https://maps.ccrpc.org/facs-ec-utility-sewer/"
  width="100%" height="400" allowfullscreen="true">
  Map showing the Sanitary and Storm Sewer on Florida Avenue Corridor Study Area of Influence.
</iframe>

## Water

The Illinois American Water Company provides water for the area. In the study
area, there are water mains along Florida Avenue and lateral service connections
to adjacent properties. There are nine fire hydrants distributed along Florida
Avenue between Lincoln Avenue and Vine Street. Water mains and fire hydrants are
also distributed in the residential subdivisions on both sides of Florida
Avenue. The University of Illinois maintains the water lines on University
property.

<iframe src="https://maps.ccrpc.org/facs-ec-utility-water/"
  width="100%" height="400" allowfullscreen="true">
  Map showing the Water Services on Florida Avenue Corridor Study Area.
</iframe>

## Street Lighting 

Florida Avenue has consistent street light coverage between Busey Avenue and
Race Street on both the north and south sides of the roadway. Florida Avenue
between Lincoln Avenue and Busey Avenue has lighting only on the south side of
the street. Florida Avenue between Race Street and Vine Street includes
lighting only on the north side of the street. Research has shown that poor
lighting can cause a significant decrease in the number of people walking and
biking. Poor lighting can also reduce the number of transit riders, since transit
riders must walk to and from bus stops.

<iframe src="https://maps.ccrpc.org/facs-ec-utility-light/"
  width="100%" height="400" allowfullscreen="true">
  Map showing the Street Lighting on Florida Avenue Corridor Study Area.
</iframe>

