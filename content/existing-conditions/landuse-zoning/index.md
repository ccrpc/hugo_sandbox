---
title: "Land Use & Zoning"
draft: false
weight: 20
---


The Florida Avenue Corridor functions as a transition between the University of
Illinois Campus District and the City of Urbana. The land uses in and around the
corridor reflect the identity of the University as a flagship campus
and the dense, walkable character of west Urbana. About a quarter of the land
surrounding the corridor is open space maintained by the City of Urbana for
public use or open space/natural areas maintained by the University for
athletes, visitors, or researchers. Except for a handful of non-residential
University structures, the remaining three quarters of the land surrounding the
corridor are residential in the form of single-family, multi-family, and
university student housing. This section provides an overview of existing land
use and zoning patterns and their implications for future transportation
improvements.

The following map illustrates current land uses and a generalized classification
of the zoning districts in the influence area for the purpose of this study.
Please refer to the [City of Urbana Zoning
Ordinance](https://www.urbanaillinois.us/zoning) for complete descriptions of
zoning ordinances and maps. About half of the influence area is zoned as
residential and the other half is zoned as Conservation-Recreation-Education.   

<iframe src="https://maps.ccrpc.org/facs-ec-landuse/"
  width="100%" height="400" allowfullscreen="true">
  Map showing existing land uses and zoning classifications within the Florida Avenue Corridor Study Area of Influence.
</iframe>

The land uses surrounding Florida Avenue have remained consistent over the last
several decades. Moving along Florida Avenue from Goodwin Avenue east to Orchard
Street, the predominate land use is institutional, which includes University
dorms, University athletic facilities, and the University President’s residence.
East of Orchard Street the land use shifts from predominately institutional to
predominately residential. Most of the residences on the north side of Florida
Avenue between Lincoln Avenue and Race Street were built in the 1920s and 30s.
The Orchard Downs housing complex, located along Orchard Street south of Florida
Avenue, was built in the 1960s and serves approximately 1,200
university-affiliated veterans, students with families, undergraduate
upperclassmen, and graduate students. The Orchard Downs complex also includes a
preschool and a community center. Private rental housing that serves the student
population is also very common in other residential areas within the influence
area, due to the proximity to campus. East of Race Street, residences built
mostly in the 1940s and 50s occupy the north and south sides of Florida Avenue,
with the exception of Blair Park at the northwest corner of Vine Street and
Florida Avenue. 

<rpc-table
url="landuse.csv"
table-title="Land Use Categories in Florida Avenue Area of Influence, 2020"
source=" Champaign County, Tax Assessor, 2020 Parcel-level Data">
</rpc-table>


The [Urbana Zoning Ordinance](https://www.urbanaillinois.us/zoning) (most
recently amended in 2018) is one of the main tools the City uses to regulate
development and the kinds of uses allowed on individual property. The purpose of
the Zoning Ordinance is to implement the policies of the City of Urbana as
expressed in the 2005 Comprehensive Plan, adopted by the City Council on April
11, 2005. [The Urbana Comprehensive Plan](http://urbanaillinois.us/node/933)
emphasizes infill development and encourages new growth in a compact and
contiguous manner. [The City is currently in the process of updating the 2005
Comprehensive Plan](https://imagineurbana.com/). 

