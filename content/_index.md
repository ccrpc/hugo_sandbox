---
title: Home
draft: false
bannerHeading: "Florida Avenue Corridor Study"
bannerText: >
  The overarching goal of this study is to identify ways to increase transportation safety, mobility, and multimodal connectivity in this high-priority east-west corridor, connecting the City of Urbana and the University of Illinois campus. 
---


## Florida Avenue Corridor Study

__Project Lead:__ Champaign-Urbana Urbanized Area Transportation Study (CUUATS)

__Project Partners:__ City of Urbana, University of Illinois at Urbana-Champaign, Champaign-Urbana Mass Transit District (MTD)

__Project Timeline:__ Fall 2020 - Spring 2022 (currently underway)

__Project Funding:__ Statewide Planning and Research funds from the Federal Highway Administration (FHWA) and the Illinois Department of Transportation (IDOT)

**Watch the following video for a short overview of the project and website:**

<iframe width="560" height="315" src="https://www.youtube.com/embed/wCiXJpTPCTY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 

**Watch the following video for a summary of the future recommendations:**

<iframe width="560" height="315" src="https://www.youtube.com/embed/fTnG_lSH5vE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The Florida Avenue Corridor Study is evaluating 1.12 miles of Florida Avenue in
the City of Urbana (from Lincoln Avenue to Vine Street) to increase
transportation safety and mobility in this high-priority, high-traffic corridor
connecting the City of Urbana and the University of Illinois flagship campus in
Urbana-Champaign. While the study is in progress, this website serves as an
evolving public repository for information about the study, public involvement
opportunities, and draft contents of the final report. Once the study is
concluded and approved, the website will serve as the final corridor study
report.

Several regional destinations are located within or adjacent to the corridor,
including the University’s Division I athletic facilities, the University
President’s residence, university student housing, a historic residential
neighborhood, and an 11-acre public park. The primary motivation for this
corridor study is based on the corridor’s central intersection of Florida Avenue
and Race Street, which was identified as one of the top 5 percent of priority
safety locations in IDOT District 5 in 2017. Top 5 percent locations exhibit
the most severe safety needs based on crashes, injuries, deaths, traffic volume
levels, and other relevant data as determined by the state. During the 5-year
safety analysis period, 28 crashes occurred at this intersection, including 3
crashes resulting in serious injuries. The intersection currently operates as a
4-way, stop-controlled intersection. The state’s analysis included one potential
safety countermeasure: conversion to a mini roundabout.

Because the City of Urbana is committed to supporting active transportation as a
healthy and environmentally responsible way to get around, this study will
consider roadway and public transit improvements to compliment the bicycle and
pedestrian improvements proposed in the City's [Bicycle Master
Plan](https://www.urbanaillinois.us/bicycle-master-plan), updated in 2016, and
[Pedestrian Master
Plan](https://ccrpc.org/documents/2020-urbana-pedestrian-master-plan-final-report/),
completed in 2020. Urbana's rates of walking, biking, and using public transit
are the highest in the region and significantly higher than state and national
averages. In 2014, Urbana was named the first [Gold-Level Bicycle Friendly
Community](http://urbanaillinois.us/BFC) in Illinois by the League of American
Bicyclists.    
